package mcib3d.tapas.core;

import fr.igred.omero.Client;
import fr.igred.omero.annotations.FileAnnotationWrapper;
import fr.igred.omero.annotations.TagAnnotationWrapper;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.OMEROServerError;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class AutoTapas {
    private String project;
    private String dataset;

    private Client omero;

    private boolean scanning = false;
    private Thread thread = null;
    private long stepScanning = 500; // in ms

    private static final String tagProcess = "to_process";
    private static final String tagProcessing = "processing";
    private static final String tagProcessed = "processed";

    private static final String IJdir = "/home/thomas/App/ImageJ";

    public AutoTapas(String project, String dataset, Client omero) {
        this.project = project;
        this.dataset = dataset;
        this.omero = omero;
    }

    public AutoTapas(String project, Client omero) {
        this.project = project;
        this.dataset = "*";
        this.omero = omero;
    }

    public void startScannning() throws AccessException, ServiceException, ExecutionException, OMEROServerError, InterruptedException {
        ProjectWrapper projectWrapper = omero.getProjects(project).get(0);
        DatasetWrapper datasetWrapper = projectWrapper.getDatasets(dataset).get(0);
        TagAnnotationWrapper tagToProcess = omero.getTags(tagProcess).get(0);
        // tags
        TagAnnotationWrapper toProcess = omero.getTags(tagProcess).get(0);
        TagAnnotationWrapper processing = omero.getTags(tagProcessing).get(0);
        TagAnnotationWrapper processed = omero.getTags(tagProcessed).get(0);
        scanning = true;
        while (scanning) {
            try {
                List<ImageWrapper> images = datasetWrapper.getImagesTagged(omero, tagToProcess);
                if (!images.isEmpty()) {
                    // get the tapas files
                    List<FileAnnotationWrapper> annotationWrappers = datasetWrapper.getFileAnnotations(omero);
                    List<FileAnnotationWrapper> annotations = annotationWrappers.stream().filter(ann -> ann.getFileName().endsWith(".tapas")).sorted(Comparator.comparing(FileAnnotationWrapper::getFileName)).collect(Collectors.toList());
                    ImageWrapper image = images.get(0);
                    image.unlink(omero, tagToProcess);
                    image.addTag(omero, processing);
                    annotations.stream().forEach(ann -> {
                        System.out.println("processing in " + dataset + " with tapas " + ann.getFileName());
                        try {
                            processFirstImageTAPAS(image, ann);
                        } catch (AccessException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    });
                    image.unlink(omero, processing);
                    image.addTag(omero, processed);
                }
                Thread.sleep(stepScanning);
            } catch (InterruptedException | AccessException | ServiceException | OMEROServerError | ExecutionException e) {
                System.out.println("Scanning "+e);
                throw new RuntimeException(e);

            }
            Thread.sleep(500);
        }


    }

    public void stopScanning() {
        scanning = false;
    }

    private void processFirstImageTAPAS(ImageWrapper image, FileAnnotationWrapper ann) throws AccessException, ExecutionException {
        // process only one image
        String listImages = image.getName();
        String fs = File.separator;
        try {
            // tags
            ann.getFile(omero, IJdir + fs + File.separator + ann.getFileName());
            // TAPAS
            TapasBatchProcess tapas = new TapasBatchProcess();
            tapas.init(IJdir + fs + ann.getFileName(), IJdir + fs + "tapas.txt");
            tapas.initBatchOmero(project, dataset, listImages, 1, 1, 1, 1);
            tapas.processAllImages();
        } catch (ServiceException | OMEROServerError | IOException e) {
            System.out.println("error tapas " + e);
            throw new RuntimeException(e);
        }
    }
}
