package mcib3d.tapas.core;
/*
 * To the extent possible under law, the OME developers have waived
 * all copyright and related or neighboring rights to this tutorial code.
 *
 * See the CC0 1.0 Universal license for details:
 *     http://creativecommons.org/publicdomain/zero/1.0/
 */

import fr.igred.omero.Client;
import fr.igred.omero.annotations.FileAnnotationWrapper;
import fr.igred.omero.annotations.TableWrapper;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.OMEROServerError;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.PixelsWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import fr.igred.omero.roi.ROIWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.measure.ResultsTable;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.plugins.OmeroPassword;
import mcib3d.utils.Logger.IJLog;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * A simple connection to an OMERO server using the Java gateway
 *
 * @author The OME Team
 */

@Deprecated
public class OmeroConnect {
    // options
    private static boolean log = false;
    private static final mcib3d.utils.Logger.AbstractLog logger = new IJLog();


    public OmeroConnect() {
    }

    public static boolean notInExcludeList(String imageName, List<String> excludeList) {
        for (String exclude : excludeList) {
            if (imageName.contains(exclude)) {
                return false;
            }
        }

        return true;
    }

    /**
     * The security context.
     */


    public void setLog(boolean log) {
        this.log = log;
    }

    @Deprecated

    public void connect() {
    }

    @Deprecated
    public void connectCredentials(String user, String pwd, String server, String port) {

    }

    public static ImageHandler getImageZ(ImageWrapper image, int t, int c, int zmin, int zmax) {
        return getImageXYZ(image, t, c, 1, 1, 0, -1, 0, -1, zmin, zmax);
    }

    public static ImageHandler getImageBin(ImageWrapper image, int t, int c, int binXY, int binZ) {
        return getImageXYZ(image, t, c, binXY, binZ, 0, -1, 0, -1, 0, -1);
    }

    public static ImageHandler getImageBinZ(ImageWrapper image, int t, int c, int zmin, int zmax, int binXY, int binZ) {
        return getImageXYZ(image, t, c, binXY, binZ, 0, -1, 0, -1, zmin, zmax);
    }

    public static ImageHandler getImage(ImageWrapper image, int t, int c) {
        return getImageXYZ(image, t, c, 1, 1, 0, -1, 0, -1, 0, -1);
    }

    public static boolean setResolutionImage(ImageWrapper imageData, double resXY, double resZ, String unit) {
        /*
        try {
            // default is um
            UnitsLength unitsLength = UnitsLength.MICROMETER;
            if (unit.equalsIgnoreCase("mm") || unit.equalsIgnoreCase("MILLIMETER"))
                unitsLength = UnitsLength.MILLIMETER;
            // get the pixels
            PixelsData pixels = imageData.asImageData().getDefaultPixels();
            pixels.setPixelSizeX(new LengthI(resXY, unitsLength));
            pixels.setPixelSizeY(new LengthI(resXY, unitsLength));
            pixels.setPixelSizeZ(new LengthI(resZ, unitsLength));
            // update database
            gateway.getUpdateService(securityContext).saveObject(pixels.asIObject());
        } catch (ServerError | DSOutOfServiceException serverError) {
            serverError.printStackTrace();
            return false;
        }
*/
        return true;

    }

    @Deprecated
    public static boolean setResolutionImageUM(ImageWrapper imageData, double resXY, double resZ) {
        /*
        try {
            // get the pixels
            PixelsData pixels = imageData.asImageData().getDefaultPixels();
            pixels.setPixelSizeX(new LengthI(resXY, UnitsLength.MICROMETER));
            pixels.setPixelSizeY(new LengthI(resXY, UnitsLength.MICROMETER));
            pixels.setPixelSizeZ(new LengthI(resZ, UnitsLength.MICROMETER));
            // update database
            gateway.getUpdateService(securityContext).saveObject(pixels.asIObject());
        } catch (ServerError | DSOutOfServiceException serverError) {
            serverError.printStackTrace();
            return false;
        }
         */

        return true;
    }

    /**
     * Load the image from OMERO
     *
     * @param image The ImageData information
     * @param t     frame, starts at 1
     * @param c     channel, starts at 1
     * @param binXY binning value in X-Y, 1,2, ...
     * @param binZ  binning value in Z, 1,2,...
     * @param XMin  for crop area, coordinate x of left crop area
     * @param XMax  for crop area, coordinate x of right crop area
     * @param YMin  for crop area, coordinate y of top  crop area
     * @param YMax  for crop area, coordinate of bottom  crop area
     * @param ZMin  for crop area, coordinate of first slice
     * @param ZMax  for crop area, coordinate of last slice
     * @return a cropped, binned image from the imageData information
     */
    public static ImageHandler getImageXYZ(ImageWrapper image, int t, int c, int binXY, int binZ, int XMin, int XMax, int YMin, int YMax, int ZMin, int ZMax) {
        Client omero = new Client();
        connectOMERO(omero);
        ImagePlus plus;
        PixelsWrapper pixels = image.getPixels();
        if (XMax < 0) XMax = pixels.getSizeX() - 1;
        if (YMax < 0) YMax = pixels.getSizeY() - 1;
        if (ZMax < 0) ZMax = pixels.getSizeZ() - 1;
        int[] xBound = new int[]{XMin, XMax};
        int[] yBound = new int[]{YMin, YMax};
        int[] zBound = new int[]{ZMin, ZMax};
        int[] cBound = new int[]{c - 1, c - 1};
        int[] tBound = new int[]{t - 1, t - 1};
        try {
            plus = image.toImagePlus(omero, xBound, yBound, cBound, zBound, tBound);
            disconnectOMERO(omero);
            ImageHandler handler = ImageHandler.wrap(plus);
            if ((binXY == 1) && (binZ == 1)) return handler;
            else {
                return handler.resample(pixels.getSizeX() / binXY, pixels.getSizeY() / binXY, pixels.getSizeZ() / binZ, -1);
            }
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            disconnectOMERO(omero);
        }

        return null;

        /*

        RawDataFacility rdf = gateway.getFacility(RawDataFacility.class);
        PixelsData pixels = image.getDefaultPixels();
        int sizeZ = pixels.getSizeZ(); // The number of z-sections.
        int sizeT = pixels.getSizeT(); // The number of timepoints.
        int sizeC = pixels.getSizeC(); // The number of channels.
        int sizeX = pixels.getSizeX(); // The number of pixels along the X-axis.
        int sizeY = pixels.getSizeY(); // The number of pixels along the Y-axis.
        Length pixelsXY = pixels.getPixelSizeX(null);
        Length pixelsZ = pixels.getPixelSizeZ(null);

        String pixelType = pixels.getPixelType();
        double resXY = 1;
        if (pixelsXY != null) resXY = pixelsXY.getValue();
        double resZ = 1;
        if (pixelsZ != null) resZ = pixelsZ.getValue();
        // check t and c
        t = Math.min(t, sizeT);
        t = Math.max(t, 1);
        c = Math.min(c, sizeC);
        c = Math.max(c, 1);
        if (XMax < 0) XMax = sizeX;
        if (YMax < 0) YMax = sizeY;
        if (ZMax < 0) ZMax = sizeZ;
        double sxfull = XMax - XMin;
        double syfull = YMax - YMin;
        double szfull = ZMax - ZMin;
        ImageHandler handler;
        switch (pixelType) {
            case "uint16":
                handler = new ImageShort("test", (int) Math.ceil(sxfull / binXY), (int) Math.ceil(syfull / binXY), (int) Math.ceil(szfull / binZ));
                break;
            case "uint8":
                handler = new ImageByte("test", (int) Math.ceil(sxfull / binXY), (int) Math.ceil(syfull / binXY), (int) Math.ceil(szfull / binZ));
                break;
            case "float":
                handler = new ImageFloat("test", (int) Math.ceil(sxfull / binXY), (int) Math.ceil(syfull / binXY), (int) Math.ceil(szfull / binZ));
                break;
            default:
                IJ.log("Cannot handle pixel type " + pixelType);
                return null;
        }
        String unit = pixelsXY.getUnit().toString();
        handler.setScale(resXY * binXY, resZ * binZ, unit);
        IJ.log("Calibration " + resXY + " " + resZ + " " + unit);
        // check plane size
        double maxSizePlane = 256000000; // maximum transfer size for OMERO
        double planeSize = (int) (sxfull * syfull);
        if (pixelType.equals("uint16")) planeSize *= 2;
        if (planeSize > maxSizePlane) {
            // divide plane in two (recursive)
            int y1 = (int) Math.floor((YMin + YMax) / 2.0);
            ImageHandler handler1 = getImageXYZ(image, t, c, binXY, binZ, XMin, XMax, YMin, y1, ZMin, ZMax);
            handler.insert(handler1, 0, 0, 0, false);
            int insert2 = handler1.sizeY;
            handler1 = getImageXYZ(image, t, c, binXY, binZ, XMin, XMax, y1, YMax, ZMin, ZMax);
            handler.insert(handler1, 0, insert2, 0, false);
        } else { // read plane from Omero
            Plane2D plane;
            for (int z = ZMin; z < ZMax; z += binZ) {
                //IJ.log("Reading plane " + z + " " + XMin + " " + YMin + " " + c + " " + t);
                plane = rdf.getTile(securityContext, pixels, z, t - 1, c - 1, XMin, YMin, (int) sxfull, (int) syfull);
                for (int x = XMin; x < XMax; x += binXY) {
                    for (int y = YMin; y < YMax; y += binXY) {
                        int xx = (x - XMin) / binXY;
                        int yy = (y - YMin) / binXY;
                        int zz = (z - ZMin) / binZ;
                        handler.setPixel(xx, yy, zz, (float) plane.getPixelValue(x - XMin, y - YMin));
                    }
                }
            }
        }

        return handler;*/
    }


    public static DatasetWrapper findDataset(String name, ProjectWrapper project, boolean strict) {
        List<DatasetWrapper> data = project.getDatasets();
        for (DatasetWrapper datasetData : data) {
            //IJ.log("testing "+datasetData.getName());
            if ((strict) && (datasetData.getName().equals(name))) {
                //IJ.log("equals "+name);
                return datasetData;
            }
            if ((!strict) && (datasetData.getName().contains(name))) {
                //IJ.log("contains "+name);
                return datasetData;
            }
        }

        return null;
    }

    public static List<DatasetWrapper> findDatasets(ProjectWrapper project) {
        return project.getDatasets();
    }


    public static ImageWrapper findOneImage(DatasetWrapper data, String name, boolean strict) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<ImageWrapper> images = data.getImages(omero);
            for (ImageWrapper imageWrapper : images) {
                if ((strict) && (imageWrapper.getName().equals(name))) {
                    disconnectOMERO(omero);
                    return imageWrapper;
                }
                if ((!strict) && (imageWrapper.getName().contains(name))) {
                    disconnectOMERO(omero);
                    return imageWrapper;
                }
            }

        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        disconnectOMERO(omero);

        return null;
    }

    public static List<ImageWrapper> findAllImages(DatasetWrapper data) {
        // GRED
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<ImageWrapper> images = data.getImages(omero);
            disconnectOMERO(omero);
            return images;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        disconnectOMERO(omero);

        return null;


        //List<ImageWrapper> imageList = new ArrayList<>();
        //BrowseFacility browse = gateway.getFacility(BrowseFacility.class);
        //Collection<ImageData> images = browse.getImagesForDatasets(securityContext, Collections.singletonList(data.getId()));
        //imageList.addAll(images);

        //return imageList;
    }


    public static List<ImageWrapper> findAllImagesExclude(DatasetWrapper data, List<String> excludeList) {
        List<ImageWrapper> imageList = findAllImages(data);
        return notInExcludeList(imageList, excludeList);
    }

    public static List<ImageWrapper> findImagesContainsName(DatasetWrapper data, String name, boolean strict) {
        List<ImageWrapper> imagesFound = new ArrayList<>();
        List<ImageWrapper> images = findAllImages(data);
        for (ImageWrapper imageData : images) {
            if (strict) {
                if (imageData.getName().equals(name))
                    imagesFound.add(imageData);
            } else {
                if (imageData.getName().contains(name))
                    imagesFound.add(imageData);
            }
        }

        return imagesFound;
    }

    public static List<ImageWrapper> findImagesContainsNameExclude(DatasetWrapper data, String name, List<String> excludeList, boolean strict) throws Exception {
        List<ImageWrapper> imageList = findImagesContainsName(data, name, strict);
        return notInExcludeList(imageList, excludeList);
    }

    public static List<ImageWrapper> notInExcludeList(List<ImageWrapper> images, List<String> excludeList) {
        List<ImageWrapper> list = new ArrayList<>();
        for (ImageWrapper imageData : images) {
            String imageName = imageData.getName();
            boolean ok = notInExcludeList(imageName, excludeList);
            if (ok) list.add(imageData);
        }

        return list;
    }

    public static ProjectWrapper findProject(String name, boolean strict) {
        List<ProjectWrapper> projects = findAllProjects();
        for (ProjectWrapper project : projects) {
            if (strict)
                if (project.getName().equals(name)) return project;
                else if (project.getName().contains(name)) return project;
        }

        return null;
    }

    public static List<ProjectWrapper> findAllProjects() {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<ProjectWrapper> projects = omero.getProjects();
            disconnectOMERO(omero);
            return projects;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }
        disconnectOMERO(omero);

        return null;
    }


    public static boolean connectOMERO(Client omero) {
        OmeroPassword password = new OmeroPassword();
        password.loadConnectionInformation();

        return connectOMERO(omero, password);
    }

    public static boolean connectOMERO(Client omero, OmeroPassword password) {
        String user = password.getUserOmero();
        String server = password.getServerOmero();
        char[] pass = password.getPasswordOmeroArrayChar();
        int port = password.getPortOmero();

        try {
            omero.connect(server, port, user, pass);
            return true;
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void disconnectOMERO(Client omero) {
        omero.disconnect();
    }

    public static ImageWrapper findOneImage(String project, String dataset, String image, boolean strict) {
        Client omero = new Client();
        ImageWrapper imageWrapper;
        try {
            connectOMERO(omero);
            imageWrapper = omero.getImages(project, dataset, image).get(0);
        } catch (ServiceException | AccessException | ExecutionException e) {
            System.out.println("Problem getting image " + image + " in " + dataset + " in " + project);
            System.out.println("ERROR " + e.getMessage());
            return null;
        } finally {
            disconnectOMERO(omero);
        }
        /*
        if (log) logger.log("Looking for project : " + project);
        ProjectWrapper projectData = findProject(project, strict);
        if (projectData == null) return null;
        if (log) logger.log("Found project : " + projectData.getName());
        DatasetWrapper datasetData = findDataset(data, projectData, strict);
        if (datasetData == null) return null;
        if (log) logger.log("Found dataset : " + datasetData.getName());
        ImageWrapper imageData = findOneImage(datasetData, image, strict);
        if (imageData == null) return null;
        if (log) logger.log("Found image : " + imageData.getName());
        */
        return imageWrapper;
    }

    public static ImageWrapper findOneImage(ImageInfo imageInfo) {
        String project = imageInfo.getProject();
        String data = imageInfo.getDataset();
        String image = imageInfo.getImage();
        boolean strict = true;

        ImageWrapper imageWrapper = findOneImage(project, data, image, strict);

        return imageWrapper;
    }

    /**
     * Retrieve image rois as ImageJ Rois(Philippe Mailly)
     *
     * @param image the image
     * @return The list of ROIs
     * @throws java.util.concurrent.ExecutionException
     * @throws omero.gateway.exception.DSOutOfServiceException
     * @throws omero.gateway.exception.DSAccessException
     */

    public static List<ij.gui.Roi> getImageRois(ImageWrapper image) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<ROIWrapper> roisWrapper = image.getROIs(omero);
            List<ij.gui.Roi> roiList = new LinkedList<>();
            ij.gui.Roi roi = null;
            for (ROIWrapper rois : roisWrapper) {
                roiList.addAll(rois.toImageJ());
            }
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void deleteImage(DatasetWrapper dataset, ImageWrapper imageData) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            dataset.removeImage(omero, imageData);
        } catch (ServiceException | AccessException | ExecutionException | OMEROServerError | InterruptedException e) {
            e.printStackTrace();
        }
        disconnectOMERO(omero);

    }

    public static boolean addImageToDataset(String projectName, String datasetName, String dir, String fileName) {
        return addImageToDataset(projectName, datasetName, dir, fileName, true);
    }


    public static boolean addImageToDataset(String projectName, String datasetName, String dir, String fileName, boolean overwrite) {
        // connect to omero
        Client omero = new Client();
        File file = new File(dir + fileName);

        try {
            connectOMERO(omero);
            ProjectWrapper projectWrapper = omero.getProjects(projectName).get(0);
            DatasetWrapper datasetWrapper = projectWrapper.getDatasets(datasetName).get(0);
            // check if overwrite
            if (overwrite) {
                datasetWrapper.importAndReplaceImages(omero, file.getPath());
            } else {
                datasetWrapper.importImage(omero, file.getPath());
            }
        } catch (ServiceException | AccessException | OMEROServerError | ExecutionException |
                 InterruptedException e) {
            return false;
        } finally {
            disconnectOMERO(omero);
        }

        return true;
    }


    public static Map<String, String> getValuePairs(ImageWrapper image) {
        Client omero = new Client();
        try {
            // GRED
            connectOMERO(omero);
            Map<String, String> map = image.getKeyValuePairs(omero);
            return map;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            omero.disconnect();
        }

        return null;



        /*
        //IJ.log("Finding pairs for keyword " + image.getName());
        Map<String, MapAnnotationData> map = new HashMap<>();
        MetadataFacility metadataFacility = gateway.getFacility(MetadataFacility.class);
        // connection
        ExperimenterData user = gateway.getLoggedInUser();
        SecurityContext ctx = new SecurityContext(user.getGroupId());
        // types
        List<Class<? extends AnnotationData>> types = new ArrayList<>();
        types.add(MapAnnotationData.class);
        // users
        long userId = gateway.getLoggedInUser().getId();
        List<Long> userIds = new ArrayList<>();
        userIds.add(userId);
        // check additional user
        //IJ.log("Add users " + addUsers.size());
        if ((addUsers != null) && (!addUsers.isEmpty())) {
            for (String users : addUsers) {
                //IJ.log("Adding user to pairs " + users);
                Long id = getUserId(users);
                if (id > 0) userIds.add(id);
            }
        }
        // get annotations
        List<AnnotationData> list = metadataFacility.getAnnotations(ctx, image, types, userIds);
        if ((list == null) || (list.isEmpty())) IJ.log("No annotations found");
        else {
            for (AnnotationData annotationData : list) {
                if (annotationData instanceof MapAnnotationData) {
                    MapAnnotationData mapAnnotationData = (MapAnnotationData) annotationData;
                    String line = mapAnnotationData.getContentAsString();
                    map.put(line, mapAnnotationData);
                }
            }
        }*/
    }

    public static List<FileAnnotationWrapper> getFileAnnotations(ImageWrapper image) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<FileAnnotationWrapper> annotations = image.getFileAnnotations(omero);
            disconnectOMERO(omero);
            return annotations;
        } catch (ExecutionException | ServiceException | AccessException e) {
            System.out.println("PB get File Annotation "+e);
        }
        disconnectOMERO(omero);

        return null;
    }

    public static FileAnnotationWrapper getFileAnnotation(ImageWrapper imageData, String name) {
        List<FileAnnotationWrapper> list = getFileAnnotations(imageData);
        for (FileAnnotationWrapper data : list) {
            if (data.getFileName().equalsIgnoreCase(name)) return data;
        }

        return null;
    }

    /*
    private OriginalFile getOriginalFile(long id) {
        ParametersI param = new ParametersI();
        param.map.put("id", omero.rtypes.rlong(id));
        IQueryPrx svc = gateway.getQueryService(securityContext);
        return (OriginalFile) svc.findByQuery("select p from OriginalFile as p where p.id = :id", param);
    }
    */

    public static File readAttachment(FileAnnotationWrapper annotation) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            File file = annotation.getFile(omero,System.getProperty("java.io.tmpdir")+File.separator+annotation.getFileName());
            return file;
        } catch (IOException | ServiceException | OMEROServerError e) {
            System.out.println("Pb reading attachment "+e);
        }

        return null;

        /*
        int INC = 262144;
        FileAnnotationData fa;
        RawFileStorePrx store = gateway.getRawFileService(securityContext);
        File file = File.createTempFile(annotation.getFileName(), ".tmp");

        OriginalFile of;
        try (FileOutputStream stream = new FileOutputStream(file)) {
            fa = annotation;
            //The id of the original file
            of = getOriginalFile(fa.getFileID());
            store.setFileId(fa.getFileID());
            int offset = 0;
            long size = of.getSize().getValue();
            try {
                for (offset = 0; (offset + INC) < size; ) {
                    stream.write(store.read(offset, INC));
                    offset += INC;
                }
            } finally {
                stream.write(store.read(offset, (int) (size - offset)));
            }
        } finally {
            store.close();
        }

        return file;

         */
    }

    public static File readAttachment(ImageInfo imageInfo, String name) {
        return readAttachment(getFileAnnotation(findOneImage(imageInfo), name));
    }


    public static String getValuePair(ImageWrapper image, String name) {
        Map<String, String> map = getValuePairs(image);
        // check if contains name
        for (String line : map.keySet()) {
            if (line.contains(name)) {
                Map<String, String> result = extractKeyValuePairs(line);
                for (String keys : result.keySet()) {
                    if (keys.equalsIgnoreCase(name)) return result.get(keys);
                }
            }
        }
        return null;
    }


    private static Map<String, String> extractKeyValuePairs(String line) {
        Map<String, String> map = new HashMap<>();
        String[] data;
        // extract individual pairs
        if (line.contains(";")) {
            data = line.split(";");
        } else data = new String[]{line};
        // extract key value
        for (String test : data) {
            String[] kvs = test.split("=");
            map.put(kvs[0], kvs[1]);
        }

        return map;
    }

    public static void addFileAnnotation(ImageWrapper image, File file) {
        addFileAnnotation(image, file, "attached");
    }

    public static void addFileAnnotation(ImageWrapper image, File file, String comment) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            image.addAndReplaceFile(omero, file);
            // TEST TABLE
            if(file.getName().endsWith(".csv")) {
                //addTableAnnotation(image, file);
            }
        } catch (ExecutionException | InterruptedException | AccessException | ServiceException | OMEROServerError e) {
            throw new RuntimeException(e);
        } finally {
            disconnectOMERO(omero);
        }

        /*
        try {
            // check if file is already attached
            List<FileAnnotationWrapper> files = image.getFileAnnotations(omero).stream().filter(a -> a.getFileName().equals(file.getName())).collect(Collectors.toList());
            if (!files.isEmpty()) {
                IJ.log("File " + file.getAbsolutePath() + " already attached to " + image.getName() + ", overwriting");
                image.unlink(omero, files.get(0));
            }
            // TEST TABLE
            if (file.getName().endsWith(".csv")) {
                ResultsTable resultsTable = ResultsTable.open(file.getAbsolutePath());
                addTableAnnotation(image, resultsTable, file.getName());
            } else {
                IJ.log("Attaching " + file.getAbsolutePath() + " to " + image.getName());
                image.addFile(omero, file);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        } catch (AccessException | ServiceException | OMEROServerError | IOException e) {
            throw new RuntimeException(e);
        }
        disconnectOMERO(omero);
        */
    }

    public static void addTableAnnotation(ImageWrapper image, ResultsTable resultsTable, String name) {
        Client omero = new Client();
        connectOMERO(omero);
        try {
            List<Roi> rois = new ArrayList<>();
            TableWrapper tableWrapper = new TableWrapper(omero, resultsTable, image.getId(), rois);
            tableWrapper.setName(name);
            IJ.log("Attaching TABLE " + name+ " to " + image.getName());
            image.addAndReplaceTable(omero, tableWrapper);
        } catch (ServiceException | AccessException | ExecutionException | InterruptedException | OMEROServerError e) {
            throw new RuntimeException(e);
        } finally {
            disconnectOMERO(omero);
        }
    }

    public static  void addTableAnnotation(ImageWrapper image, File file){
        try {
            ResultsTable rt = ResultsTable.open(file.getPath());
            if(rt != null){
                addTableAnnotation(image, rt, file.getName().replace(".csv",".table"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}