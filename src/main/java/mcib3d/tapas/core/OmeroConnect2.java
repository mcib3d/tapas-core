package mcib3d.tapas.core;
/*
 * To the extent possible under law, the OME developers have waived
 * all copyright and related or neighboring rights to this tutorial code.
 *
 * See the CC0 1.0 Universal license for details:
 *     http://creativecommons.org/publicdomain/zero/1.0/
 */

import fr.igred.omero.Client;
import fr.igred.omero.annotations.FileAnnotationWrapper;
import fr.igred.omero.annotations.MapAnnotationWrapper;
import fr.igred.omero.annotations.TableWrapper;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.OMEROServerError;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.PixelsWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import fr.igred.omero.roi.ROIWrapper;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.measure.ResultsTable;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.plugins.OmeroPassword;
import mcib3d.utils.Logger.IJLog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * A simple connection to an OMERO server using the Java gateway
 *
 * @author The OME Team
 */
public class OmeroConnect2 {
    // options
    private static boolean log = false;
    private static final mcib3d.utils.Logger.AbstractLog logger = new IJLog();

    // TEST
    static Client omero = null;
    static String sessionId = "";
    static String hostOmero;
    static int portOmero;


    public OmeroConnect2() {
        if ((omero == null) || (!omero.isConnected())) createConnection();

    }

    private static void createConnection() {
        omero = new Client();
        OmeroPassword password = new OmeroPassword();
        password.loadConnectionInformation();
        String user = password.getUserOmero();
        String server = password.getServerOmero();
        char[] pass = password.getPasswordOmeroArrayChar();
        int port = password.getPortOmero();
        System.out.println("Connection to OMERO for "+user);
        try {
            omero.connect(server, port, user, pass, password.getGroup());
            //omero.connect(server, port, user, pass);
            sessionId = omero.getSessionId();
            hostOmero = server;
            portOmero = port;
        } catch (ServiceException e) {
            System.out.println("Problem connecting to OMERO " + e);
        }
        try {
            System.out.println("Connected to omero " + omero.isConnected() + " " + omero.getSessionId());
        } catch (ServiceException e) {
            System.out.println("Problem with OMERO session " + e);
        }
    }

    public static Client getOmeroClient() {
        if ((omero==null)||(!omero.isConnected())) createConnection();

        return omero;
    }

    public boolean notInExcludeList(String imageName, List<String> excludeList) {
        for (String exclude : excludeList) {
            if (imageName.contains(exclude)) {
                return false;
            }
        }

        return true;
    }

    /**
     * The security context.
     */


    public void setLog(boolean log) {
        this.log = log;
    }

    @Deprecated
    public void connect() {
    }

    @Deprecated
    public void connectCredentials(String user, String pwd, String server, String port) {
    }

    public ImageHandler getImageZ(ImageWrapper image, int t, int c, int zmin, int zmax) {
        return getImageXYZ(image, t, c, 1, 1, 0, -1, 0, -1, zmin, zmax);
    }

    public ImageHandler getImageBin(ImageWrapper image, int t, int c, int binXY, int binZ) {
        return getImageXYZ(image, t, c, binXY, binZ, 0, -1, 0, -1, 0, -1);
    }

    public ImageHandler getImageBinZ(ImageWrapper image, int t, int c, int zmin, int zmax, int binXY, int binZ) {
        return getImageXYZ(image, t, c, binXY, binZ, 0, -1, 0, -1, zmin, zmax);
    }

    public ImageHandler getImage(ImageWrapper image, int t, int c) {
        return getImageXYZ(image, t, c, 1, 1, 0, -1, 0, -1, 0, -1);
    }

    /**
     * Load the image from OMERO
     *
     * @param image The ImageData information
     * @param t     frame, starts at 1
     * @param c     channel, starts at 1
     * @param binXY binning value in X-Y, 1,2, ...
     * @param binZ  binning value in Z, 1,2,...
     * @param XMin  for crop area, coordinate x of left crop area
     * @param XMax  for crop area, coordinate x of right crop area
     * @param YMin  for crop area, coordinate y of top  crop area
     * @param YMax  for crop area, coordinate of bottom  crop area
     * @param ZMin  for crop area, coordinate of first slice
     * @param ZMax  for crop area, coordinate of last slice
     * @return a cropped, binned image from the imageData information
     */
    public ImageHandler getImageXYZ(ImageWrapper image, int t, int c, int binXY, int binZ, int XMin, int XMax, int YMin, int YMax, int ZMin, int ZMax) {
        ImagePlus plus;
        PixelsWrapper pixels = image.getPixels();
        if (XMax < 0) XMax = pixels.getSizeX() - 1;
        if (YMax < 0) YMax = pixels.getSizeY() - 1;
        if (ZMax < 0) ZMax = pixels.getSizeZ() - 1;
        int[] xBound = new int[]{XMin, XMax};
        int[] yBound = new int[]{YMin, YMax};
        int[] zBound = new int[]{ZMin, ZMax};
        int[] cBound = new int[]{c - 1, c - 1};
        int[] tBound = new int[]{t - 1, t - 1};
        try {
            plus = image.toImagePlus(omero, xBound, yBound, cBound, zBound, tBound);
            disconnectOMERO(omero);
            ImageHandler handler = ImageHandler.wrap(plus);
            if ((binXY == 1) && (binZ == 1)) return handler;
            else {
                return handler.resample(pixels.getSizeX() / binXY, pixels.getSizeY() / binXY, pixels.getSizeZ() / binZ, -1);
            }
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    public DatasetWrapper findDataset(String name, ProjectWrapper project, boolean strict) {
        List<DatasetWrapper> data = project.getDatasets();
        for (DatasetWrapper datasetData : data) {
            //IJ.log("testing "+datasetData.getName());
            if ((strict) && (datasetData.getName().equals(name))) {
                //IJ.log("equals "+name);
                return datasetData;
            }
            if ((!strict) && (datasetData.getName().contains(name))) {
                //IJ.log("contains "+name);
                return datasetData;
            }
        }

        return null;
    }

    public List<DatasetWrapper> findDatasets(ProjectWrapper project) {
        return project.getDatasets();
    }


    public ImageWrapper findOneImage(DatasetWrapper data, String name, boolean strict) {
        try {
            List<ImageWrapper> images = data.getImages(omero);
            for (ImageWrapper imageWrapper : images) {
                if ((strict) && (imageWrapper.getName().equals(name))) {
                    return imageWrapper;
                }
                if ((!strict) && (imageWrapper.getName().contains(name))) {
                    return imageWrapper;
                }
            }

        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<ImageWrapper> findAllImages(DatasetWrapper data) {
        try {
            List<ImageWrapper> images = data.getImages(omero);
            disconnectOMERO(omero);
            return images;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<ImageWrapper> findAllImagesExclude(DatasetWrapper data, List<String> excludeList) {
        List<ImageWrapper> imageList = findAllImages(data);
        return notInExcludeList(imageList, excludeList);
    }

    public List<ImageWrapper> findImagesContainsName(DatasetWrapper data, String name, boolean strict) {
        List<ImageWrapper> imagesFound = new ArrayList<>();
        List<ImageWrapper> images = findAllImages(data);
        for (ImageWrapper imageData : images) {
            if (strict) {
                if (imageData.getName().equals(name))
                    imagesFound.add(imageData);
            } else {
                if (imageData.getName().contains(name))
                    imagesFound.add(imageData);
            }
        }

        return imagesFound;
    }

    public List<ImageWrapper> findImagesContainsNameExclude(DatasetWrapper data, String name, List<String> excludeList, boolean strict) throws Exception {
        List<ImageWrapper> imageList = findImagesContainsName(data, name, strict);
        return notInExcludeList(imageList, excludeList);
    }

    public List<ImageWrapper> notInExcludeList(List<ImageWrapper> images, List<String> excludeList) {
        List<ImageWrapper> list = new ArrayList<>();
        for (ImageWrapper imageData : images) {
            String imageName = imageData.getName();
            boolean ok = notInExcludeList(imageName, excludeList);
            if (ok) list.add(imageData);
        }

        return list;
    }

    public ProjectWrapper findProject(String name, boolean strict) {
        List<ProjectWrapper> projects = findAllProjects();
        for (ProjectWrapper project : projects) {
            if (strict) {
                if (project.getName().equals(name)) return project;
            } else if (project.getName().contains(name)) return project;
        }

        return null;
    }

    public List<ProjectWrapper> findAllProjects() {
        try {
            List<ProjectWrapper> projects = getOmeroClient().getProjects();
            return projects;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Deprecated
    public boolean connectOMERO(Client omero) {
        OmeroPassword password = new OmeroPassword();
        password.loadConnectionInformation();

        return connectOMERO(omero, password);
    }


    @Deprecated
    public boolean connectOMERO(Client omero, OmeroPassword password) {
        String user = password.getUserOmero();
        String server = password.getServerOmero();
        char[] pass = password.getPasswordOmeroArrayChar();
        int port = password.getPortOmero();

        try {
            omero.connect(server, port, user, pass);
            return true;
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Deprecated
    public void disconnectOMERO(Client omero) {
        omero.disconnect();
    }

    public void disconnect() {
        omero.disconnect();
        omero = null;
    }

    public ImageWrapper findOneImage(String project, String dataset, String image, boolean strict) {
        ImageWrapper imageWrapper;
        try {
            imageWrapper = omero.getImages(project, dataset, image).get(0);
        } catch (ServiceException | AccessException | ExecutionException e) {
            System.out.println("Problem getting image " + image + " in " + dataset + " in " + project);
            System.out.println("ERROR " + e.getMessage());
            return null;
        }

        return imageWrapper;
    }

    public ImageWrapper findOneImage(ImageInfo imageInfo) {
        String project = imageInfo.getProject();
        String data = imageInfo.getDataset();
        String image = imageInfo.getImage();
        boolean strict = true;

        ImageWrapper imageWrapper = findOneImage(project, data, image, strict);

        return imageWrapper;
    }

    /**
     * Retrieve image rois as ImageJ Rois(Philippe Mailly)
     *
     * @param image the image
     * @return The list of ROIs
     */

    public List<Roi> getImageRois(ImageWrapper image) {
        try {
            List<ROIWrapper> roisWrapper = image.getROIs(omero);
            List<Roi> roiList = new LinkedList<>();
            for (ROIWrapper rois : roisWrapper) {
                roiList.addAll(rois.toImageJ());
            }
            return roiList;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    public void deleteImage(DatasetWrapper dataset, ImageWrapper imageData) {
        try {
            dataset.removeImage(omero, imageData);
        } catch (ServiceException | AccessException | ExecutionException | OMEROServerError | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean addImageToDataset(String projectName, String datasetName, String dir, String fileName) {
        return addImageToDataset(projectName, datasetName, dir, fileName, true);
    }


    public boolean addImageToDataset(String projectName, String datasetName, String dir, String fileName, boolean overwrite) {
        File file = new File(dir + fileName);

        try {
            ProjectWrapper projectWrapper = omero.getProjects(projectName).get(0);
            DatasetWrapper datasetWrapper = projectWrapper.getDatasets(datasetName).get(0);
            // check if overwrite
            if (overwrite) {
                datasetWrapper.importAndReplaceImages(omero, file.getPath());
            } else {
                datasetWrapper.importImage(omero, file.getPath());
            }
        } catch (ServiceException | AccessException | OMEROServerError | ExecutionException |
                 InterruptedException e) {
            return false;
        }

        return true;
    }


    public Map<String, String> getValuePairs(ImageWrapper image) {
        try {
            Map<String, String> map = image.getKeyValuePairs(omero);
            return map;
        } catch (ServiceException | AccessException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<FileAnnotationWrapper> getFileAnnotations(ImageWrapper image) {
        //Client omero = new Client();
        //connectOMERO(omero);
        try {
            List<FileAnnotationWrapper> annotations = image.getFileAnnotations(omero);
            //disconnectOMERO(omero);
            return annotations;
        } catch (ExecutionException | ServiceException | AccessException e) {
            System.out.println("PB get File Annotation " + e);
        }
        //disconnectOMERO(omero);

        return null;
    }

    public FileAnnotationWrapper getFileAnnotation(ImageWrapper imageData, String name) {
        List<FileAnnotationWrapper> list = getFileAnnotations(imageData);
        for (FileAnnotationWrapper data : list) {
            if (data.getFileName().equalsIgnoreCase(name)) return data;
        }

        return null;
    }

    public List<String> getTags(ImageWrapper imageWrapper) {
        List<String> tags = new ArrayList<>();
        try {
            tags = imageWrapper.getTags(getOmeroClient()).stream().map(t -> t.getName()).collect(Collectors.toList());
        } catch (ServiceException | AccessException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        return tags;
    }

    public boolean hasTag(ImageWrapper imageWrapper, String tag) {
        List<String> tags = getTags(imageWrapper);
        return tags.stream().anyMatch(t -> t.equalsIgnoreCase(tag));
    }


    public File readAttachment(FileAnnotationWrapper annotation) {
        try {
            File file = annotation.getFile(omero, System.getProperty("java.io.tmpdir") + File.separator + annotation.getFileName());
            return file;
        } catch (IOException | ServiceException | OMEROServerError e) {
            System.out.println("Pb reading attachment " + e);
        }

        return null;

    }

    public File readAttachment(ImageInfo imageInfo, String name) {
        return readAttachment(getFileAnnotation(findOneImage(imageInfo), name));
    }


    public String getValuePair(ImageWrapper image, String name) {
        Map<String, String> map = getValuePairs(image);
        // check if contains name
        for (String line : map.keySet()) {
            if (line.equals(name)) {
                return map.get(line);
            }
        }
        return null;
    }

    public MapAnnotationWrapper getMapAnnotation(ImageWrapper image, String name) {
        List<MapAnnotationWrapper> list = null;
        try {
            list = image.getMapAnnotations(getOmeroClient());
            // check if contains name
            for (MapAnnotationWrapper line : list) {
                if (line.asMapAnnotationData().getContentAsString().startsWith(name + "="))
                    return line;
            }
        } catch (ServiceException | AccessException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    public MapAnnotationWrapper extractMapAnnotation(ImageWrapper image, List<MapAnnotationWrapper> list, String name) {
        // check if contains name
        for (MapAnnotationWrapper line : list) {
            if (line.asMapAnnotationData().getContentAsString().startsWith(name + "="))
                return line;
        }
        return null;
    }

//    private Map<String, String> extractKeyValuePairs(String line) {
//        Map<String, String> map = new HashMap<>();
//        String[] data;
//        // extract individual pairs
//        if (line.contains(";")) {
//            data = line.split(";");
//        } else data = new String[]{line};
//        // extract key value
//        for (String test : data) {
//            System.out.println("TESTING KV "+test);
//            String[] kvs = test.split("=");
//            map.put(kvs[0], kvs[1]);
//        }
//
//        return map;
//    }

    public void addFileAnnotation(ImageWrapper image, File file) {
        addFileAnnotation(image, file, "attached");
    }

    public void addFileAnnotation(ImageWrapper image, File file, String comment) {
        try {
            image.addAndReplaceFile(omero, file);
            // TEST TABLE
            if (file.getName().endsWith(".csv")) {
                //addTableAnnotation(image, file);
            }
        } catch (ExecutionException | InterruptedException | AccessException | ServiceException | OMEROServerError e) {
            throw new RuntimeException(e);
        }
    }

    public void addFileAnnotationDataset(DatasetWrapper dataset, File file) {
        addFileAnnotationDataset(dataset, file, "attached");
    }

    public void addFileAnnotationDataset(DatasetWrapper dataset, File file, String comment) {
        try {
            dataset.addAndReplaceFile (omero, file);
            // TEST TABLE
            if (file.getName().endsWith(".csv")) {
                //addTableAnnotation(image, file);
            }
        } catch (ExecutionException | InterruptedException | AccessException | ServiceException | OMEROServerError e) {
            throw new RuntimeException(e);
        }
    }


    public void addTableAnnotation(ImageWrapper image, ResultsTable resultsTable, String name) {
        try {
            List<Roi> rois = new ArrayList<>();
            System.out.println("" + getOmeroClient() + " " + resultsTable + " " + image.getId() + " " + rois);
            TableWrapper tableWrapper = new TableWrapper(getOmeroClient(), resultsTable, image.getId(), rois);
            System.out.println("TABLE WRAPPER " + tableWrapper);
            System.out.println(tableWrapper.getColumnCount() + " " + tableWrapper.getRowCount());
            tableWrapper.setName(name);
            image.addAndReplaceTable(getOmeroClient(), tableWrapper);
        } catch (ServiceException | AccessException | ExecutionException | OMEROServerError | InterruptedException e) {
            System.out.println("Error " + e);
        }
    }

    public void addTableAnnotation(ImageWrapper image, File file) {
        try {
            ResultsTable rt = ResultsTable.open(file.getPath());
            if (rt != null) {
                addTableAnnotation(image, rt, file.getName().replace(".csv", ".table"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}