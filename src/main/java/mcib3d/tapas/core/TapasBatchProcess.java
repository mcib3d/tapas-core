package mcib3d.tapas.core;


import fr.igred.omero.Client;
import fr.igred.omero.annotations.TagAnnotationWrapper;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.OMEROServerError;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.plugin.Duplicator;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessorIJ;
import mcib3d.utils.Logger.AbstractLog;
import mcib3d.utils.Logger.IJLog;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class TapasBatchProcess {
    public static String version = "0.8.0";
    // list of global links (tapas variables), pb if many instances of this class ?
    private static final Map<String, ImageInfo> links = new HashMap<>();
    private List<ImageInfo> allImages;
    private List<TapasProcessingAbstract> processings;
    private TapasProcessorAbstract processorAbstract = new TapasProcessorIJ();
    private Map<String, String> plugins;
    // additional connection information
    private final List<Long> addUsers;
    private List<Long> addGroups; // not used

    // TEST
    //static Client omero = null;

    // OMERO connection
    //String[] credentials = null;

    private final AbstractLog log = new IJLog();
    private final AbstractLog error = new IJLog();

    public TapasBatchProcess() {
        addUsers = new ArrayList<>();
    }


    @Deprecated
    public static String analyseKeyValueOld(String s, ImageWrapper image, OmeroConnect connect) {
        if (!s.contains("?key_")) return null;
        int pos0 = s.indexOf("?key");
        int pos1 = s.indexOf("_", pos0);
        if (pos1 < 0) return null;
        int pos2 = s.indexOf("?", pos1);
        if (pos2 < 0) return null;
        String keyValue = s.substring(pos0, pos2 + 1);
        String key = s.substring(pos1 + 1, pos2);
        //IJ.log("checking value for key "+key);
        String value = new OmeroConnect2().getValuePair(image, key);
        if (value == null) return null;

        return s.replace(keyValue, value);
    }

    private static ImageInfo analyseLinkName(String s, ImageInfo info) {
        if (!s.contains("LINK_")) return null;
        System.out.println("Analysing link name " + s);
        int pos0 = s.indexOf("LINK_");
        int pos1 = s.indexOf("_", pos0);
        if (pos1 < 0) return null;
        int pos2 = s.length();
        String link = s.substring(pos1 + 1, pos2);
        System.out.println("Found link name " + link);

        return links.get(link);
    }

    public static ImagePlus inputImage(ImageInfo info) {
        return inputImage(info, info.getProject(), info.getDataset(), info.getImage(), info.getC(), info.getT());
    }

    public static ImagePlus inputImage(ImageInfo info, String project, String dataset, String image, int c, int t) {
        ImagePlus plus = null;
        if (info.isOmero()) {
            try {
                // GRED
                Client omero = OmeroConnect2.getOmeroClient();
                // project
                List<ProjectWrapper> projects = omero.getProjects(project);
                if (projects.isEmpty()) {
                    IJ.log("Could not find project " + project);
                    return null;
                }
                ProjectWrapper projectWrapper = projects.get(0);
                // dataset
                List<DatasetWrapper> datasets = projectWrapper.getDatasets(dataset);
                if (datasets.isEmpty()) {
                    IJ.log("Could not find dataset " + dataset + " in project " + project);
                    return null;
                }
                DatasetWrapper datasetWrapper = datasets.get(0);
                // image
                List<ImageWrapper> images = datasetWrapper.getImages(omero, image);
                if (images.isEmpty()) {
                    IJ.log("Could not find image " + image + " in dataset " + dataset);
                    return null;
                }
                ImageWrapper imageWrapper = images.get(0);
                plus = imageWrapper.toImagePlus(omero, null, null, new int[]{c - 1, c - 1}, null, new int[]{t - 1, t - 1});
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else { // use bioformats
            ImageInfo info2 = new ImageInfo(info.getRootDir(), project, dataset, image, c, t);
            IJ.log("Loading with BioFormats: " + info2.getFilePath());
            plus = BioformatsReader.OpenImagePlus(info2.getFilePath(), info2.getC() - 1, info2.getT() - 1);
            if (plus == null) {
                IJ.log("Could not load " + info2.getFilePath());
                return null;
            }
            plus.setTitle(image);
        }

        return plus;
    }

    public static boolean outputImage(ImagePlus input, ImageInfo info, String project, String dataset, String name) {
        if (info.isFile()) {
            IJ.log("Saving to FILES");
            return outputImageFiles(input, info, project, dataset, name);
        } else {
            IJ.log("Importing to OMERO");
            return outputImageOMERO(input, info, project, dataset, name);
        }
    }

    private static boolean outputImageFiles(ImagePlus input, ImageInfo info, String project, String dataset, String name) {
        boolean ok = true;
        ImageInfo info2 = new ImageInfo(info.getRootDir(), project, dataset, name, info.getC(), info.getT());
        String path2 = info2.getFilePath();
        // check if file exists
        File file = new File(path2);
        if (file.exists()) {
            IJ.log("File  " + path2 + " already exists, deleting");
            file.delete();
        }
        if (!saveFile(input, path2)) {
            IJ.log("Pb saving " + path2);
            ok = false;
        } else IJ.log("Saved  " + path2);

        return ok;
    }

    private static boolean outputImageOMERO(ImagePlus input, ImageInfo info, String project, String dataset, String name) {
        boolean debug = false;
        boolean ok = true;

        // save temporary file
        String dirTmp = System.getProperty("java.io.tmpdir");
        String pathOmero = dirTmp + File.separator + name;
        if (!saveFile(input, pathOmero)) {
            IJ.log("Pb saving temp " + pathOmero);
            ok = false;
        }
        // GRED
        if(debug) System.out.println("Connecting to omero");
        Client omero = new OmeroConnect2().getOmeroClient();
        if(debug) System.out.println("Connected to omero "+omero);

        // project
        List<ProjectWrapper> projects = null;
        try {
            projects = omero.getProjects(project);
            if (projects.isEmpty()) {
                IJ.log("Could not find project " + project);
                return false;
            }
            ProjectWrapper projectWrapper = projects.get(0);
            if(debug) System.out.println("Found project "+projectWrapper);
            // dataset
            List<DatasetWrapper> datasets = projectWrapper.getDatasets(dataset);
            if (datasets.isEmpty()) {
                IJ.log("Could not find dataset " + dataset + " in project " + project);
                return false;
            }
            DatasetWrapper datasetWrapper = datasets.get(0);
            if(debug) System.out.println("Found dataset "+datasetWrapper);

            // importing file
            datasetWrapper.importAndReplaceImages(omero, pathOmero);

            // delete tmp file
            Files.delete(new File(pathOmero).toPath());

        } catch (ServiceException | AccessException | ExecutionException | IOException | OMEROServerError |
                 InterruptedException e) {
            System.out.println("Error importing into omero " + e);
            return false;
        }

        return ok;
    }

    public static void addTags(String project, String dataset, String image, String[] tags) {
        boolean debug = false;
        if(debug) System.out.println("Searching image "+project+"/"+dataset+"/"+image);
        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        ImageWrapper imageWrapper = omeroConnect2.findOneImage(project, dataset, image, true);
        if(image == null) {
            System.out.println("Image "+project+"/"+dataset+"/"+image+" not found");
            return;
        }
        if(debug) System.out.println("Found image "+imageWrapper.getId());
        Client omero = omeroConnect2.getOmeroClient();
        try {
            List<TagAnnotationWrapper> tagsImage = imageWrapper.getTags(omero);
            for (String tag : tags) {
                if(debug) System.out.println("Testing tag " + tag);
                List<TagAnnotationWrapper> tagAnnotationWrappers = omero.getTags(tag.trim());
                if(debug) System.out.println("Listed tags " + tagAnnotationWrappers.size());
                if (!tagAnnotationWrappers.isEmpty()) {
                    TagAnnotationWrapper tagToAdd = tagAnnotationWrappers.get(0);
                    if(debug) System.out.println("Adding tag " + tagToAdd.getName() + " to image " + imageWrapper.getName() + " " + imageWrapper.getId());
                    boolean already = tagsImage.stream().anyMatch(ta -> ta.getId() == tagToAdd.getId());
                    // check if tag already there ?
                    if (!already)
                        imageWrapper.addTag(omero, tagAnnotationWrappers.get(0));
                }
            }
        } catch (ServiceException | AccessException | ExecutionException | OMEROServerError e) {
            System.out.println("Error add tag " + e);
        }
    }

    private static boolean saveFile(ImagePlus input, String path) {
        FileSaver saver = new FileSaver(input);
        boolean saveOk;
        if (input.getNSlices() > 1) {
            saveOk = saver.saveAsTiffStack(path);
        } else {
            saveOk = saver.saveAsTiff(path);
        }

        return saveOk;
    }

    public static ImagePlus duplicatePlus(ImagePlus plus) {
        if(plus == null) return null;
        String title = plus.getTitle();
        Duplicator duplicator = new Duplicator();
        ImagePlus copy = duplicator.run(plus);
        copy.setTitle(title);

        return copy;

    }

    @Deprecated
    public static ImagePlus getImageFromFileParameters(String dir, String file, ImageInfo current) {
        ImageHandler img;
        if (file.contains("LINK_")) {
            // core link
            ImageInfo imageInfo = analyseLinkName(file, current);
            if (imageInfo == null) return null;
            try {
                // GRED
                Client omero = new OmeroConnect2().getOmeroClient();

                // project
                List<ProjectWrapper> projects = omero.getProjects(imageInfo.getProject());
                if (projects.isEmpty()) {
                    IJ.log("Could not find project " + imageInfo.getProject());
                    omero.disconnect();
                    return null;
                }
                ProjectWrapper projectWrapper = projects.get(0);
                // dataset
                List<DatasetWrapper> datasets = projectWrapper.getDatasets(imageInfo.getDataset());
                if (datasets.isEmpty()) {
                    IJ.log("Could not find dataset " + imageInfo.getDataset() + " in project " + imageInfo.getProject());
                    omero.disconnect();
                    return null;
                }
                DatasetWrapper datasetWrapper = datasets.get(0);
                // image
                List<ImageWrapper> images = datasetWrapper.getImages(omero, imageInfo.getImage());
                if (images.isEmpty()) {
                    IJ.log("Could not find image " + imageInfo.getImage() + " in dataset " + imageInfo.getDataset());
                    omero.disconnect();
                    return null;
                }
                ImageWrapper imageWrapper = images.get(0);

                return imageWrapper.toImagePlus(omero, null, null, new int[]{imageInfo.getC() - 1, imageInfo.getC() - 1}, null, new int[]{imageInfo.getT() - 1, imageInfo.getT() - 1});
            } catch (AccessException | ServiceException | ExecutionException e) {
                e.printStackTrace();
            }
        } else {
            // file
            String nameF = TapasBatchUtils.analyseFileName(file, current);
            String dirF = TapasBatchUtils.analyseDirName(dir);
            IJ.log("Opening " + dirF + nameF);
            return IJ.openImage(dirF + nameF);
        }

                        /*
                OmeroConnect connect = new OmeroConnect();
                connect.connect();
                ImageData image = connect.findOneImage(imageInfo);
                if (image == null) return null;
                img = (ImageInt) connect.getImage(image, imageInfo.getT(), imageInfo.getC());
                connect.disconnect();
                return img.getImagePlus();
            } catch (Exception e) {
                IJ.log("Pb with image " + imageInfo.getImage());
                e.printStackTrace();
            }
        } else {
            // file
            String nameF = TapasBatchUtils.analyseFileName(file, current);
            String dirF = TapasBatchUtils.analyseDirName(dir);
            IJ.log("Opening " + dirF + nameF);
            return IJ.openImage(dirF + nameF);
        }

        return null;
        */
        return null;
    }

    public static void setlink(String name, ImageInfo info) {
        links.put(name, info);
    }

    public static ImageInfo getLink(String name) {
        ImageInfo imageInfo = analyseLinkName(name, null);

        return imageInfo;
    }

    public static void listLinks() {
        links.keySet().forEach(key -> System.out.println(key + " " + links.get(key)));
    }

    public static List<TapasProcessingAbstract> readProcessings(String file, Map<String, String> plugins) {
        List<TapasProcessingAbstract> tapasProcessings = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            String[] info = new String[2];
            TapasProcessingAbstract processing = null;
            while (line != null) {
                if ((!line.startsWith("//")) && (!line.isEmpty())) {
                    //String info[] = line.split(":"); // FIXME, finds first : , in case : in dir name (Windows)
                    int pos0 = line.indexOf(":");
                    if (pos0 == -1) IJ.log("Pb line does not contain \":\" " + line);
                    info[0] = line.substring(0, pos0);
                    info[1] = line.substring(pos0 + 1);
                    if ((info[0].isEmpty()) || (info[1].isEmpty())) IJ.log("Pb with tapas processing line " + line);
                    else {
                        if (info[0].equalsIgnoreCase("process")) {
                            processing = createProcess(info[1], plugins);
                            if (processing == null) {
                                IJ.log("No tapas with name " + info[1]);
                                return null;
                            }
                            tapasProcessings.add(processing);

                        } else if (!info[0].equalsIgnoreCase("processor")) {
                            if (!processing.setParameter(info[0].trim(), info[1].trim())) {
                                IJ.log("Pb when processing parameters : " + info[0] + " with value " + info[1]);
                                return null;
                            }
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            IJ.log("Process File not found " + e.getMessage());
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            IJ.log("Process File pb " + e.getMessage());
            e.printStackTrace();
            return null;
        }

        return tapasProcessings;
    }

    public static TapasProcessingAbstract createProcess(String moduleName, Map<String, String> plugins) {
        try {
            moduleName = moduleName.trim();
            String processClass = plugins.get(moduleName);
            IJ.log("Creating process " + moduleName + ":" + processClass);
            if (processClass == null) return null;
            Object object = Class.forName(processClass).getConstructor().newInstance();
            TapasProcessingAbstract processing = (TapasProcessingAbstract) object;
            return processing;
        } catch (ClassNotFoundException e) {
            IJ.log("Error Class Not Found " + plugins.get(moduleName));
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            IJ.log("Access Class Not Found " + plugins.get(moduleName));
            e.printStackTrace();
        } catch (InstantiationException e) {
            IJ.log("Error cannot create object " + plugins.get(moduleName));
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            IJ.log("Error with constructor of object " + plugins.get(moduleName));
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            IJ.log("Error method not found of object " + plugins.get(moduleName));
            e.printStackTrace();
        }

        return null;
    }

    public static Map<String, String> readPluginsFile(String file, boolean verbose) {
        Map<String, String> map = new HashMap<>();
        IJ.log("Reading tapas file " + file);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            int c = 0;
            while (line != null) {
                c++;
                int idx = line.indexOf("//");
                if ((idx < 0) && (!line.isEmpty())) { // strange error on linux ??
                    String info[] = line.split(":");
                    if (info.length != 2) {
                        IJ.log("Pb for tapas plugins with line " + c + ":" + line + " ");
                        return null;
                    }
                    map.put(info[0].trim(), info[1].trim());
                    if (verbose) IJ.log("Found plugin " + info[0].trim() + " " + info[1].trim());
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return map;
    }

    public TapasProcessorAbstract getProcessor() {
        return processorAbstract;
    }

    public void setProcessor(TapasProcessorAbstract processorAbstract) {
        this.processorAbstract = processorAbstract;
    }

    public boolean init(String process, String tapas) {
        if (!readPlugins(tapas)) return false;
        if (!readProcessing(process, plugins)) return false;

        return true;
    }

    public boolean processAllImages() {
        processorAbstract.init(processings, allImages);
        return processorAbstract.processAllImages();
    }

    public boolean processOneImage(ImageInfo info) {
        processorAbstract.init(processings);

        return (processorAbstract.processOneImage(info) != null);
    }

    public boolean setProcessing(List<TapasProcessingAbstract> process) {
        processings = process;

        return true;
    }

    private List<DatasetWrapper> getInitDatasets(String project, String dataset) throws ServiceException, AccessException, ExecutionException {
        List<DatasetWrapper> datasetsInit = new ArrayList<>();
        
        Client omero = OmeroConnect2.getOmeroClient();
        
        // project
        ProjectWrapper projectWrapper = omero.getProjects(project).get(0);
        if(projectWrapper == null){
            IJ.log("Could not find project " + project);
            return null;
        }

        // dataset
        if (dataset.equals("*")) { // all datasest, // FIXME to remove ?
            List<DatasetWrapper> datasets = projectWrapper.getDatasets();
            if (datasets.isEmpty()) {
                IJ.log("Could not find any datasets in project " + project);
                return null;
            }
            datasetsInit.addAll(datasets);
        } else { // one or many datasets
            if (!dataset.contains(",")) { // only one dataset name
                List<DatasetWrapper> datasets = projectWrapper.getDatasets(dataset);
                if (datasets.isEmpty()) {
                    IJ.log("Could not find dataset " + dataset + " in project " + project);
                    return null;
                }
                datasetsInit.add(datasets.get(0));
            } else { // many datasets name separated by ,
                String[] datasetStrings = dataset.split(",");
                for (String datasetString : datasetStrings) {
                    List<DatasetWrapper> datasets = projectWrapper.getDatasets(datasetString);
                    if (datasets.isEmpty()) {
                        IJ.log("Could not find dataset " + datasetString + " in project " + project);
                    }
                   else datasetsInit.add(datasets.get(0));
                }
            }
        }

        return datasetsInit;
    }

    private List<File> getDatasetsInProjectFile(File project, String dataset) {
        List<File> datasets = new ArrayList<>();
        File[] files = project.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                if (dataset.equals("*")) {
                    datasets.add(files[i]);
                } else {
                    if (files[i].getName().contains(dataset))
                        datasets.add(files[i]);
                }
            }
        }

        return datasets;
    }

    private List<ImageWrapper> getInitImagesInDataset(DatasetWrapper dataset, String image, List<String> exclude, boolean strict) {
        final List<ImageWrapper> imagesInit = new ArrayList<>();

        try {
            Client omero = OmeroConnect2.getOmeroClient();
        
            // all images
            if (image.equals("*")) {
                imagesInit.addAll(dataset.getImages(omero));
                // all images
            } else {
                if (!image.contains(",")) { // only one image name
                    imagesInit.addAll(dataset.getImages(omero, image));
                } else { // many images name separated by ,
                    String[] imagesStrings = image.split(",");
                    for (String imageString : imagesStrings) {
                        imagesInit.addAll(dataset.getImages(omero, imageString.trim()));
                    }
                }
            }
            if (exclude.isEmpty()) {
                return imagesInit;
            }
            // all images + exclude
            else {
                //return omeroConnect2.notInExcludeList(imagesInit, exclude);
            }
        } catch (AccessException | ServiceException | ExecutionException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private List<File> getFilesInDatasetFile(File dataset, String image, List<String> exclude) {
        List<File> images = new ArrayList<>();
        File[] files = dataset.listFiles();
        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        for (File file : files) {
            if (file.isDirectory()) continue;
            String fileName = file.getName();
            if ((exclude != null) && (!exclude.isEmpty()) && (!omeroConnect2.notInExcludeList(fileName, exclude)))
                continue;
            if ((!image.equals("*")) && (!fileName.contains(image))) continue;
            images.add(file);
        }
        return images;
    }

    public List<File> getAllImagesInProjectDatasetFile(File project, String dataset, String image, List<String> exclude) {
        List<File> allFiles = new ArrayList<>();
        List<File> datasets = getDatasetsInProjectFile(project, dataset);
        for (File data : datasets) {
            List<File> files = getFilesInDatasetFile(data, image, exclude);
            allFiles.addAll(files);
        }

        return allFiles;
    }

    private List<ImageInfo> initOmeroInfoFromImages(String project, DatasetWrapper dataset, List<ImageWrapper> images, int c0, int c1, int t0, int t1) {
        List<ImageInfo> infos = new ArrayList<>();
        for (ImageWrapper image : images)
            for (int c = c0; c <= c1; c++) {
                for (int t = t0; t <= t1; t++) {
                    ImageInfo info = new ImageInfo(project, dataset.getName(), image.getName(), c, t);
                    infos.add(info);
                }
            }
        return infos;
    }

    public void initBatchOmero(String project, String datasetName, String image, int c0, int c1, int t0, int t1) throws ServiceException, AccessException, ExecutionException {
        List<String> exclude = new ArrayList<>(0);
        initBatchOmero(project, datasetName, image, exclude, c0, c1, t0, t1, true);
    }

    public void initBatchOmero(String project, String datasetName, String image, List<String> exclude, int c0, int c1, int t0, int t1, boolean strict) throws ServiceException, AccessException, ExecutionException {
        allImages = new ArrayList<>();
        // get all datasets
        List<DatasetWrapper> datasets = getInitDatasets(project, datasetName);
        if ((datasets == null) || (datasets.isEmpty())) error.log("No datasets found");
        for (DatasetWrapper dataset : datasets) {
            log.log("Searching in dataset " + dataset);
            List<ImageWrapper> images = getInitImagesInDataset(dataset, image, exclude, strict);
            if (images == null) error.log("No images found");
            else {
                log.log("Tapas found " + images.size() + " images to process");
                allImages.addAll(initOmeroInfoFromImages(project, dataset, images, c0, c1, t0, t1));
            }
        }
    }

    public void initBatchFiles(String root, String project, String datasetName, String image, int c0, int c1, int t0, int t1) {
        allImages = new ArrayList<>();
        List<String> files = new ArrayList<>();
        // get all files
        if (!root.endsWith(File.separator)) root = root.concat(File.separator);
        File folder = new File(root + project + File.separator + datasetName);
        // all files
        if (image.equals("*")) {
            File[] fileList = folder.listFiles();
            for (File file : fileList) {
                if (file.isFile())
                    files.add(file.getName());
            }
        }
        // list of images separated by ,
        else if (image.contains(",")) {
            String[] fileList = image.split(",");
            for (String file : fileList) files.add(file);
        }
        // only one image
        else {
            files.add(image);
        }
        // create image informations
        for (String file : files) {
            for (int t = t0; t <= t1; t++) {
                for (int c = c0; c <= c1; c++) {
                    log.log("Adding : " + root + "/" + project + "/" + datasetName + "/" + file + "-" + c + "-" + t);
                    allImages.add(new ImageInfo(root, project, datasetName, file, c, t));
                }
            }
        }
    }


    public boolean readProcessing(String file, Map<String, String> plugins) {
        List<TapasProcessingAbstract> tapasProcessings = readProcessings(file, plugins);
        if (tapasProcessings == null) return false;
        if (processings == null) processings = tapasProcessings;

        return true;
    }

    public static TapasProcessorAbstract getProcessor(String file) {
        TapasProcessorAbstract processorAbstract = new TapasProcessorIJ();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            String info[] = new String[2];
            while (line != null) {
                if ((!line.startsWith("//")) && (!line.isEmpty())) {
                    //String info[] = line.split(":"); // FIXME, finds first : , in case : in dir name (Windows)
                    int pos0 = line.indexOf(":");
                    if (pos0 == -1) IJ.log("Pb line does not contain \":\" " + line);
                    info[0] = line.substring(0, pos0);
                    info[1] = line.substring(pos0 + 1);
                    if ((info[0].isEmpty()) || (info[1].isEmpty())) IJ.log("Pb with tapas processing line " + line);
                    else {
                        if (info[0].equalsIgnoreCase("processor")) {
                            Class cls = Class.forName(info[1]);
                            Object object = cls.newInstance();
                            processorAbstract = (TapasProcessorAbstract) object;
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            IJ.log("Process File not found " + e.getMessage());
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            IJ.log("Process File pb " + e.getMessage());
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return processorAbstract;
    }

    public boolean readPlugins(String file) {
        Map<String, String> map = readPluginsFile(file, true);
        if (map == null) return false;
        plugins = map;

        return true;
    }


    private void TESTJAR() {
        File file = new File("c:\\myjar.jar");
        URL url = null;
        try {
            url = file.toURI().toURL();
            URL[] urls = new URL[]{url};
            ClassLoader cl = new URLClassLoader(urls);
            Class cls = cl.loadClass("com.mypackage.myclass");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}
