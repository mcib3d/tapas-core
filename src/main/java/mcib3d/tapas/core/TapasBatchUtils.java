package mcib3d.tapas.core;

import fr.igred.omero.Client;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class TapasBatchUtils {
    public static File getTapasMenuFile() {
        // FIXME will be deprecated in 0.7, will be in tapas folder with name .tpm
        File tapasFile = new File(IJ.getDirectory("imagej") + File.separator + "tapas.txt");
        IJ.log("Checking tapas file " + tapasFile.getAbsolutePath());
        if (!tapasFile.exists()) {
            IJ.log("No tapas file found : " + tapasFile.getPath());
            tapasFile = new File(IJ.getDirectory("plugins") + File.separator + "TAPAS" + File.separator + "tapas.tpm");
            IJ.log("Checking tapas file " + tapasFile.getAbsolutePath());
            if (!tapasFile.exists()) {
                IJ.log("No tapas file found : " + tapasFile.getPath());
                IJ.log("Cannot run TAPAS. Please check installation.");
                return null;
            }
            IJ.log("Tapas file found : " + tapasFile.getPath());
        }

        return tapasFile;
    }

    public static String analyseDirName(String s) {
        if (s == null) return s;
        if (s.isEmpty()) return s;
        String dir = s;
        String home = System.getProperty("user.home");
        String ij = IJ.getDirectory("imagej");
        String tmp = System.getProperty("java.io.tmpdir");
        // we want these dir to NOT ends with /
        if (home.endsWith(File.separator)) home = home.substring(0, home.length() - 1);
        if (ij.endsWith(File.separator)) ij = ij.substring(0, ij.length() - 1);
        if (tmp.endsWith(File.separator)) tmp = tmp.substring(0, tmp.length() - 1);
        // but we want final dir name to ends with /
        if (dir.contains("?home?")) dir = dir.replace("?home?", home);
        if (dir.contains("?Home?")) dir = dir.replace("?Home?", home);
        if (dir.contains("?ij?")) dir = dir.replace("?ij?", ij);
        if (dir.contains("?IJ?")) dir = dir.replace("?IJ?", ij);
        if (dir.contains("?tmp?")) dir = dir.replace("?tmp?", tmp);
        if (!dir.endsWith(File.separator)) dir = dir.concat(File.separator);

        return dir;
    }

    public static String analyseFileName(String s, ImageInfo info) {
        String file = s;
        if ((s == null) || (s.isEmpty())) return s;
        // keywords ?...?
        if (file.contains("?project?")) {
            file = file.replace("?project?", info.getProject());
            return analyseFileName(file, info);
        }
        if (file.contains("?Project?")) {
            file = file.replace("?Project?", info.getProject());
            return analyseFileName(file, info);
        }
        if (file.contains("?dataset?")) {
            file = file.replace("?dataset?", info.getDataset());
            return analyseFileName(file, info);
        }
        if (file.contains("?Dataset?")) {
            file = file.replace("?Dataset?", info.getDataset());
            return analyseFileName(file, info);
        }
        if (file.contains("?name?")) { // deprecated --> ?image?
            file = file.replace("?name?", info.getImage());
            return analyseFileName(file, info);
        }
        if (file.contains("?image?")) {
            file = file.replace("?image?", info.getImage());
            return analyseFileName(file, info);
        }
        if (file.contains("?Image?")) {
            file = file.replace("?Image?", info.getImage());
            return analyseFileName(file, info);
        }
        if (file.contains("?channel?")) {
            file = file.replace("?channel?", "" + info.getC());
            return analyseFileName(file, info);
        }
        if (file.contains("?channel+1?")) {
            file = file.replace("?channel+1?", "" + (info.getC() + 1));
            return analyseFileName(file, info);
        }
        if (file.contains("?channel-1?")) {
            file = file.replace("?channel-1?", "" + (info.getC() - 1));
            return analyseFileName(file, info);
        }
        if (file.contains("?frame?")) {
            file = file.replace("?frame?", "" + info.getT());
            return analyseFileName(file, info);
        }
        if (file.contains("?frame+1?")) {
            file = file.replace("?frame+1?", "" + (info.getT() + 1));
            return analyseFileName(file, info);
        }
        if (file.contains("?frame-1?")) {
            file = file.replace("?frame-1?", "" + (info.getT() - 1));
            return analyseFileName(file, info);
        }
        // REPLACE
        String replaced = replace(file);
        if (replaced != null) return replaced;

        return file;
    }

    public static String analyseStringKeywords(String s, ImageInfo info) {
        // first analyse names
        String res = analyseFileName(s, info);
        // system directories
        String home = System.getProperty("user.home");
        String ij = IJ.getDirectory("imagej");
        String tmp = System.getProperty("java.io.tmpdir");
        // we want these dir to NOT ends with /
        if (home.endsWith(File.separator)) home = home.substring(0, home.length() - 1);
        if (ij.endsWith(File.separator)) ij = ij.substring(0, ij.length() - 1);
        if (tmp.endsWith(File.separator)) tmp = tmp.substring(0, tmp.length() - 1);
        // analyse dir names
        res = res.replace("?home?", home);
        res = res.replace("?ij?", ij);
        res = res.replace("?tmp?", tmp);

        return res;
    }


    public static int analyseChannelFrameName(String s, ImageInfo info) {
        if (s.toLowerCase().contains("?channel?")) return info.getC();
        else if (s.toLowerCase().contains("?frame?")) return info.getT();
        else return Integer.parseInt(s);
    }


    public static boolean attach(ImageInfo info, File file, String project, String dataset, String name) {
        boolean ok;
        if (info.isFile()) { // if file copy in same dataset directory
            ok = attachFiles(info, file, project, dataset);
        } else {
            ok = attachOMERO(file, project, dataset, name);
        }

        return ok;
    }

    public static boolean attachFiles(ImageInfo info, File file, String project, String dataset) {
        String name = file.getName();
        String path = info.getRootDir() + project + File.separator + dataset + File.separator + name;
        // new 0.6.3, put in a folder "attachments"
        File attachFolder = new File(info.getRootDir() + project + File.separator + dataset + File.separator + "attachments" + File.separator);
        if (!attachFolder.exists()) {
            IJ.log("Creating folder " + attachFolder + " to store attachments");
            attachFolder.mkdir();
        }
        path = attachFolder.getPath() + File.separator + name;
        try {
            IJ.log("Attaching to FILES");
            File file2 = new File(path);
            // delete if exist
            if (file2.exists()) {
                IJ.log("File " + file2.getPath() + " exists. Overwriting");
                file2.delete();
            }
            Files.copy(file.toPath(), file2.toPath());
        } catch (IOException e) {
            IJ.log("Could not copy " + file.getPath() + " to " + path);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean attachOMERO(File file, String project, String dataset, String image) {
        try {
            // GRED
            Client omero = new OmeroConnect2().getOmeroClient();

            // project
            List<ProjectWrapper> projects = omero.getProjects(project);
            if (projects.isEmpty()) {
                IJ.log("Could not find project " + project);
                omero.disconnect();
                return false;
            }
            ProjectWrapper projectWrapper = projects.get(0);
            // dataset
            List<DatasetWrapper> datasets = projectWrapper.getDatasets(dataset);
            if (datasets.isEmpty()) {
                IJ.log("Could not find dataset " + dataset + " in project " + project);
                omero.disconnect();
                return false;
            }
            DatasetWrapper datasetWrapper = datasets.get(0);
            // image
            List<ImageWrapper> images = datasetWrapper.getImages(omero, image);
            if (images.isEmpty()) {
                IJ.log("Could not find image " + image + " in dataset " + dataset);
                omero.disconnect();
                return false;
            }
            ImageWrapper imageWrapper = images.get(0);

            IJ.log("Attaching to OMERO");
            imageWrapper.addFile(omero, file);
            omero.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getKey(String keyS, ImageInfo info) {
        if (!keyS.contains("KEY_")) return keyS;
        // for OMERO
        if (info.isOmero()) {
            try {
                OmeroConnect2 omeroConnect2 = new OmeroConnect2();
                ImageWrapper imageData = omeroConnect2.findOneImage(info.getProject(), info.getDataset(), info.getImage(), true);
                return analyseKeyValue(keyS, imageData, info);
            } catch (Exception e) {
                IJ.log("Pb with key " + keyS);
            }
        }
        // for FILES
        else {
            String value = KeyValueManager.getKeyValue(info, keyS);
            if (value != null) return value;
        }

        return keyS;
    }

    public static String analyseKeyValue(String keyS, ImageWrapper image, ImageInfo info) {
        if (!keyS.contains("KEY_")) return null;
        int pos0 = keyS.indexOf("KEY_");
        int pos1 = keyS.indexOf("_", pos0);
        if (pos1 < 0) return null;
        int pos2 = keyS.length();
        String key = keyS.substring(pos1 + 1, pos2);
        // analysing key value
        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        String keyValue = analyseFileName(key, info);
        String value = omeroConnect2.getValuePair(image, keyValue);
        if (value == null) {
            IJ.log("No key " + keyS);
            return null;
        }

        return value;
    }

    public static String replace(String text) {
        if (!text.contains("_REPLACE")) return text;
        int pos0 = text.indexOf("_REPLACE");
        int pos1 = text.indexOf("(", pos0);
        if (pos1 < 0) return null;
        int pos2 = text.lastIndexOf(")");
        String expr = text.substring(pos1 + 1, pos2);
        String[] evals = expr.split(",");

        return evals[0].trim().replace(evals[1].trim(), evals[2].trim());
    }

    public static void main(String[] args) {
        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        Client omero = omeroConnect2.getOmeroClient();
        IJ.log("Connection " + omero.getGateway());
        omero.disconnect();
    }
}
