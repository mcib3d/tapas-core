package mcib3d.tapas.core;

import ij.IJ;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class TapasDocumentation {
    Map<String, List<String>> categories;
    Map<String, String> docs; // class name and documentation text
    public static final String DEFAULT = "Documentation not provided.";

    public static void main(String[] args) {
        TapasDocumentation doc = new TapasDocumentation();
        doc.loadDocumentation("/home/thomas/App/ImageJ/plugins/TAPAS/tapasDocumentation.txt");
        doc.printCategories();
    }

    public TapasDocumentation() {
        docs = new HashMap<>();
        categories = new HashMap<>();
        categories.put("Other", new ArrayList<>());
    }

    public void setDocumentation(String className, String doc) {
        docs.put(className, doc);
    }

    public String getDocumentation(String className) {
        String doc = docs.get(className);
        if (doc == null) return DEFAULT;
        else return doc;
    }

    public List<String> getCategories(){
        List<String > cat = new ArrayList<>();
        cat.addAll(categories.keySet());
        return cat;
    }

    public List<String> getPlugingsFromCategory(String category){
        return categories.get(category);
    }

    public void printCategories() {
        for (String category : categories.keySet()) {
            System.out.println("CATEGORY " + category);
            for (String module : categories.get(category)) {
                System.out.println(module);
                System.out.println(docs.get(module));
            }
        }
    }

    public void checkNoDocPlugin(List<String> plugins){
        // get plugins not associated with a documentation
        List<String> nodoc = plugins.stream().filter(p -> !docs.containsKey(p)).collect(Collectors.toList());
        nodoc.stream().forEach(p->System.out.println("NODOC "+p));
        // associate these plugins to category 'Other'
        categories.get("Other").addAll(nodoc);
    }

    public void loadDocumentation(String fileName) {
        String data2;
        String currentClass = null;
        String currentDoc = "";
        String currentCategory = "Other";
        categories.put(currentCategory, new LinkedList<>());

        IJ.log("Reading tapas documentation file " + fileName);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            while (line != null) {
                boolean comment = line.trim().startsWith("//");
                if ((!comment) && (!line.isEmpty())) {
                    line = line.trim();
                    if (line.startsWith("class:")) { // class
                        String[] info = line.split(":");
                        data2 = info[1].trim(); // doc or category name
                        // create new one
                        currentDoc = "";
                        currentClass = data2;
                        categories.get(currentCategory).add(currentClass);
                    } else if (line.startsWith("category:")) {
                        String[] info = line.split(":");
                        data2 = info[1].trim(); // doc or category name
                        currentCategory = data2;
                        if (!categories.containsKey(currentCategory)) {
                            categories.put(currentCategory, new LinkedList<>());
                        }
                    } else { // documentation line
                        if (currentClass != null) {
                            currentDoc = currentDoc.concat(line + "\n");
                            setDocumentation(currentClass, currentDoc);
                        }
                    }
                }
                line = reader.readLine();
            }
            // update last class
            if (currentClass != null) {
                setDocumentation(currentClass, currentDoc);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
