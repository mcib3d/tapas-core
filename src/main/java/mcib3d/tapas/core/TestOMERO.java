package mcib3d.tapas.core;

import fr.igred.omero.Client;
import fr.igred.omero.annotations.FileAnnotationWrapper;
import fr.igred.omero.annotations.MapAnnotationWrapper;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.OMEROServerError;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.Roi;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.ImageProcessor;
import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DIntLabelImage;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.plugins.OmeroPassword;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class TestOMERO {
    public static void main(String[] args) throws Exception {
        testOMERO();
    }

    public static void testOMERO() throws AccessException, ServiceException, ExecutionException {
        OmeroPassword password = new OmeroPassword("thomas.boudier", "tB2023MOrpH", "bioimage.france-bioinformatique.fr", "4064");
        password.saveConnectionInformation();
        password.loadConnectionInformation();

        Client client = OmeroConnect2.getOmeroClient();
        //client.connect("bioimage.france-bioinformatique.fr", 4064, "thomas.boudier", password.getPasswordOmeroArrayChar());
        System.out.println("Bonjour "+client.getCurrentGroupId());

        //System.out.println(""+Prefs.getPrefsDir());

        ProjectWrapper project = client.getProjects("TAPAS").get(0);
        DatasetWrapper dataset = project.getDatasets("FISH").get(0);
        ImageWrapper image = dataset.getImages(client).get(0);

        TapasBatchProcess tapas = new TapasBatchProcess();
        tapas.initBatchOmero(project.getName(),  dataset.getName(), "*", 1, 1, 1, 1);
        //tapas.init("/home/thomas/ownCloud/I3S/Presentations/IPMC-06112023/Data/FISH/00_segmentNucleus.tapas", "/home/thomas/App/ImageJ/plugins/TAPAS/tapas.tpm");
        //tapas.processAllImages();

        client.disconnect();

    }

    static private void analyzeImageName(OmeroConnect2 omeroConnect2, ImageWrapper imageWrapper, String desc, String ext) {
        try {
            List<MapAnnotationWrapper> list = imageWrapper.getMapAnnotations(omeroConnect2.getOmeroClient());
            Client client = omeroConnect2.getOmeroClient();
            String imageName = imageWrapper.getName().replace(ext, "").trim();
            String[] kv1 = imageName.split("_");
            String[] des = desc.split("_");
                for (int i = 0; i < des.length; i++) {
                    // TEST kv exist
                    //String kv = omeroConnect2.getValuePair(imageWrapper, des[i]);
                    MapAnnotationWrapper kv=omeroConnect2.extractMapAnnotation(imageWrapper,list,des[i]);
                    if ((kv != null)) {
                        System.out.println("Found KEY " + des[i] + " with value " + kv);
                    } else {
                        if (i < kv1.length) System.out.println(des[i] + "_" + kv1[i]);
                        else System.out.println(des[i] + "_" + "NA");
                        // kv
                        if (i < kv1.length) {
                            imageWrapper.addPairKeyValue(client, des[i], kv1[i]);
                        } else imageWrapper.addPairKeyValue(client, des[i], "NA");
                    }
                }
            } catch (AccessException | ServiceException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

        static private void processTAPAS(String project, String dataset, String image, FileAnnotationWrapper ann, Client omero) throws AccessException, ExecutionException {
        try {
            ann.getFile(omero, "/home/thomas/" + ann.getFileName());
            // TAPAS
            TapasBatchProcess tapas = new TapasBatchProcess();
            tapas.init("/home/thomas/" + ann.getFileName(), "/home/thomas/App/ImageJ/tapas.txt");
            tapas.initBatchOmero(project, dataset, image, 1, 1, 1, 1);
            tapas.processAllImages();
        } catch (IOException | ServiceException | OMEROServerError e) {
            throw new RuntimeException(e);
        }
    }

    static private Map<Integer, Roi> computeRoi3D(Object3DInt object3DInt) {
        Map<Integer, Roi> rois = new HashMap<>();
        BoundingBox box = object3DInt.getBoundingBox();
        Object3DIntLabelImage labelImage = new Object3DIntLabelImage(object3DInt);
        ImageHandler label = labelImage.getLabelImage(255);
        for (int z = box.zmin; z <= box.zmax; z++) {
            ImageHandler crop = label.crop3D("crop", 0, label.sizeX - 1, 0, label.sizeY - 1, z, z);
            ImagePlus plus = crop.getImagePlus();
            ImageProcessor processor = plus.getProcessor();
            processor.setThreshold(1, 255, ImageProcessor.NO_LUT_UPDATE);
            ThresholdToSelection tts = new ThresholdToSelection();
            tts.setup("", plus);
            tts.run(processor);
            if (plus.getRoi() != null) rois.put(z, plus.getRoi());
        }

        return rois;
    }


}
