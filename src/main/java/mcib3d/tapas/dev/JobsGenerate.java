package mcib3d.tapas.dev;

import fr.igred.omero.Client;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import mcib3d.tapas.core.OmeroConnect2;
import mcib3d.tapas.plugins.OmeroPassword;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class JobsGenerate {
    // server
    private int nbImagesJob = 4;
    private String pathFiji = "/home/tboudier/Fiji.app";
    private String exeFiji = "imagej-launcher-6.0.3-20201117.205912-15-linux64.sh";
    private String pathToProcessing = "/home/tboudier/NEUROCELL";
    // local
    private String outputDir = "/home/thomas/AMU/Projects/NEUROCELL/CODE/Jobs/";
    private String nameSubmit = "submitAll.sh";
    // parameter for each job
    private int jobCpus = 8;
    private int jobMem = 4; // Gb NOT USED
    private int maxTimeM = 0; // minutes NOT USED
    private int maxTimeImage = 60; // minutes per image
    private int jobPause = 60; // pause between two submit (sec)
    // data
    private String root = "OMERO";
    private String project;
    private String dataset;
    private List<String> imageList;
    private String processing;
    private String server = "volta";
    private String admin = "a318";

    public void generateScripts() {
        // open file for submit_all file
        try {
            int nJobs = (int) Math.ceil((double) imageList.size() / (double) nbImagesJob);
            BufferedWriter bw = new BufferedWriter(new FileWriter(outputDir + nameSubmit));
            // new path for processing
            String processName = new File(processing).getName();
            // separate the image with ,
            String macro = "run(\"TAPAS BATCH\", \"root=[?root?] project=[?project?] dataset=[?dataset?] image=[?name?] channel=[1-1] frame=[1-1] processing=[?processing?]\")";
            // write the macros first
            for (int i = 0; i < nJobs; i++) {
                int nbImaJob = 1;
                String imageMacro = imageList.get(i * nbImagesJob);
                String fileName = "image_" + (i + 1);
                for (int j = 1; j < nbImagesJob; j++) {
                    int ij = i * nbImagesJob + j;
                    if (ij >= imageList.size()) break;
                    String name = imageList.get(ij);
                    imageMacro = imageMacro.concat("," + name);
                    nbImaJob++;
                }
                String macroTmp = macro.replace("?root?", root)
                        .replace("?project?", project)
                        .replace("?dataset?", dataset)
                        .replace("?name?", imageMacro)
                        .replace("?processing?", pathToProcessing + File.separator + processName);
                macroTmp = macroTmp.concat("\neval(\"script\", \"System.exit(0);\");");
                //System.out.println("Macro " + macroTmp);
                writeFile(outputDir, fileName, ".ijm", macroTmp);
                int maxH = (int) Math.ceil((maxTimeImage*nbImaJob) / 60.0);
                String shell = "#!/bin/sh\n" +
                        "#SBATCH -J Job_" + (i + 1) + "\n" +
                        "#SBATCH -p " + server + "\n" +
                        "#SBATCH --gres=gpu:1\n" +
                        "#SBATCH --gres-flags=enforce-binding # active l’affinité CPU:GPU\n" +
                        "#SBATCH -n " + jobCpus + "\n" +
                        "#SBATCH -A " + admin + "\n" +
                        "#SBATCH -t " + maxH + ":00:00\n" +
                        "#SBATCH -o %x.out\n" +
                        "#SBATCH -e %x.err\n" +
                        "# load modules\n" +
                        "module purge\n" +
                        "module load userspace/all\n" +
                        "module load cuda/11.6\n" +
                        "module load oracle-jdk/1.8.0_171\n" +
                        "module load python3/3.8.6\n" +
                        pathFiji + File.separator + exeFiji + " --headless --console -macro " + fileName + ".ijm\n";
                //System.out.println("Shell " + shell);
                writeFile(outputDir, fileName, ".sh", shell);
                bw.write("sbatch " + fileName + ".sh\nsleep " + jobPause + "\n");
                bw.flush();
            }
            bw.close();
        } catch (IOException e) {
            System.out.println("PB jobs generate " + e);
            e.printStackTrace();
        }
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public void setImageList(List<String> list) {
        this.imageList = list;
    }

    public void setProcessing(String processing) {
        this.processing = processing;
    }

    private void writeFile(String dir, String file, String ext, String macro) {
        //System.out.println("Writing " + dir + " " + file + " " + ext + " " + macro);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir + File.separator + file + ext));
            bw.write(macro);
            bw.close();
        } catch (IOException e) {
            IJ.log("Pb with file " + dir + " " + file + " : " + e);
        }
    }

    public int getNbImagesJob() {
        return nbImagesJob;
    }

    public void setNbImagesJob(int nbImagesJob) {
        this.nbImagesJob = nbImagesJob;
    }

    public String getPathFiji() {
        return pathFiji;
    }

    public void setPathFiji(String pathFiji) {
        this.pathFiji = pathFiji;
    }

    public String getExeFiji() {
        return exeFiji;
    }

    public void setExeFiji(String exeFiji) {
        this.exeFiji = exeFiji;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public String getNameSubmit() {
        return nameSubmit;
    }

    public void setNameSubmit(String nameSubmit) {
        this.nameSubmit = nameSubmit;
    }

    public String getPathToProcessing() {
        return pathToProcessing;
    }

    public void setPathToProcessing(String pathToProcessing) {
        this.pathToProcessing = pathToProcessing;
    }

    public int getJobCpus() {
        return jobCpus;
    }

    public void setJobCpus(int jobCpus) {
        this.jobCpus = jobCpus;
    }


    public int getMaxTimeImage() {
        return maxTimeImage;
    }

    public void setMaxTimeImage(int maxTimeImage) {
        this.maxTimeImage = maxTimeImage;
    }

    public int getJobPause() {
        return jobPause;
    }

    public void setJobPause(int jobPause) {
        this.jobPause = jobPause;
    }

    public static void main(String[] args) throws AccessException, ServiceException, ExecutionException {
        OmeroPassword password = new OmeroPassword("thomas.b", "testpass7", "inmed.centuri-engineering.univ-amu.fr", "24064");
        password.saveConnectionInformation();
        password.loadConnectionInformation();
        ProjectWrapper projectWrapper;
        DatasetWrapper datasetWrapper;

        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        Client client = omeroConnect2.getOmeroClient();

        // TEST OMERO CONNECT
        projectWrapper = omeroConnect2.findProject("Reelin", true);
        datasetWrapper = omeroConnect2.findDataset("Median_IC", projectWrapper, true);
        List<String> images = datasetWrapper.getImages(client).stream().map(ImageWrapper::getName).collect(Collectors.toList());

        JobsGenerate jobsGenerate = new JobsGenerate();
        jobsGenerate.setProject(projectWrapper.getName());
        jobsGenerate.setDataset(datasetWrapper.getName());
        jobsGenerate.setImageList(images);
        jobsGenerate.setProcessing("tapas-cellpose-reelin.txt");
        jobsGenerate.setServer("volta");
        jobsGenerate.setAdmin("a318");
        jobsGenerate.setPathFiji("/home/tboudier/Fiji.app");
        jobsGenerate.setJobCpus(8);
        jobsGenerate.setMaxTimeImage(4);
        jobsGenerate.setNbImagesJob(4);
        jobsGenerate.setOutputDir("/home/thomas/AMU/Projects/NEUROCELL/CODE/Jobs/");
        //jobsGenerate.generateScripts();

        omeroConnect2.disconnect();
    }

}
