package mcib3d.tapas.dev;

import mcib3d.tapas.core.TapasBatchProcess;

import javax.swing.*;
import java.awt.*;

public class Jobs_Scripts extends JFrame {
    // GUI
    private JPanel panel1;
    private JTextField textFieldPathFiji;
    private JTextField textFieldExeFiji;
    private JTextField textFieldPathScript;
    private JTextField textFieldOutput;
    private JTextField textFieldNbImages;
    private JTextField textFieldPause;
    private JTextField textFieldCPU;
    private JTextField textFieldMachine;
    private JTextField textFieldTimeH;
    private JTextField textFieldID;
    private JButton generateScriptsButton;
    private JTextField textFieldSubmit;
    private JTextField textFieldProject;
    private JTextField textFieldDataset;
    // job
    JobsGenerate jobsGenerate;

    public Jobs_Scripts() throws HeadlessException {
        panel1.setMinimumSize(new Dimension(800, 600));
        panel1.setPreferredSize(new Dimension(800, 600));
        setContentPane(panel1);
        setTitle("TAPAS JOBS " + TapasBatchProcess.version);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        init(new JobsGenerate());
    }

    public Jobs_Scripts(JobsGenerate jobsGenerate) throws HeadlessException {
        panel1.setMinimumSize(new Dimension(800, 600));
        panel1.setPreferredSize(new Dimension(800, 600));
        setContentPane(panel1);
        setTitle("TAPAS JOBS " + TapasBatchProcess.version);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        init(jobsGenerate);
    }



    public void init(JobsGenerate job) throws HeadlessException {
        // job
        jobsGenerate = job;
        // init
        textFieldPathFiji.setText(jobsGenerate.getPathFiji());
        textFieldExeFiji.setText(jobsGenerate.getExeFiji());
        textFieldPathScript.setText(jobsGenerate.getPathToProcessing());
        textFieldOutput.setText(jobsGenerate.getOutputDir());
        textFieldSubmit.setText(jobsGenerate.getNameSubmit());
        textFieldNbImages.setText("" + jobsGenerate.getNbImagesJob());
        textFieldPause.setText("" + jobsGenerate.getJobPause());
        textFieldCPU.setText("" + jobsGenerate.getJobCpus());
        textFieldMachine.setText("" + jobsGenerate.getServer());
        textFieldTimeH.setText("" + jobsGenerate.getMaxTimeImage());
        textFieldID.setText("" + jobsGenerate.getAdmin());
        textFieldProject.setText(""+jobsGenerate.getProject());
        textFieldDataset.setText(""+jobsGenerate.getDataset());

        // generate scripts
        generateScriptsButton.addActionListener(e -> generate());
    }

    private void generate() {
        System.out.println("Generate");
        jobsGenerate.setPathFiji(textFieldPathFiji.getText());
        jobsGenerate.setExeFiji(textFieldExeFiji.getText());
        jobsGenerate.setPathToProcessing(textFieldPathScript.getText());
        jobsGenerate.setOutputDir(textFieldOutput.getText());
        jobsGenerate.setNameSubmit(textFieldSubmit.getText());
        jobsGenerate.setNbImagesJob(Integer.parseInt(textFieldNbImages.getText()));
        jobsGenerate.setJobCpus(Integer.parseInt(textFieldCPU.getText()));
        jobsGenerate.setJobPause(Integer.parseInt(textFieldPause.getText()));
        jobsGenerate.setMaxTimeImage(Integer.parseInt(textFieldTimeH.getText()));
        jobsGenerate.setAdmin(textFieldID.getText());
        jobsGenerate.setServer(textFieldMachine.getText());

        jobsGenerate.generateScripts();


    }

}
