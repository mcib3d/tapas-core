package mcib3d.tapas.plugins;

import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.io.FileSaver;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import mcib3d.tapas.core.OmeroConnect2;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class OmeroMacro implements MacroExtension, PlugIn {
    private final static String OMERO_LOAD = "OMERO_LoadImage";
    private final static String OMERO_SAVE = "OMERO_SaveImage";
    private final static String OMERO_PROJECTS = "OMERO_ListProjects";
    private final static String OMERO_DATASETS = "OMERO_ListDatasets";
    private final static String OMERO_IMAGES = "OMERO_ListImages";
    private final static String OMERO_RESULTS = "OMERO_AttachResults";

    private static OmeroMacro omeroMacro = null;

    private static final String delimiter = "\n";

    public OmeroMacro() {
        if (IJ.macroRunning()) Functions.registerExtensions(omeroMacro);
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case OMERO_LOAD: {
                String[] argsOmero = extractArgsOmero(args);
                if (argsOmero != null) {
                    ImagePlus plus = OmeroLoad(argsOmero[0], argsOmero[1], argsOmero[2]);
                    WindowManager.setTempCurrentImage(plus);
                    plus.show();
                }
                break;
            }
            case OMERO_SAVE: {
                String[] argsOmero = extractArgsOmero(args);
                if (argsOmero != null) {
                    ImagePlus plus = WindowManager.getCurrentImage();
                    if (plus != null) {
                        OmeroSave(plus, argsOmero[0], argsOmero[1], argsOmero[2]);
                    }
                }
                break;
            }
            case OMERO_RESULTS: {
                String[] argsOmero = extractArgsOmeroName(args);
                if (argsOmero != null) {
                    ResultsTable table = ResultsTable.getResultsTable();
                    if (table != null) {
                        OmeroAttach(table, argsOmero[0], argsOmero[1], argsOmero[2], argsOmero[3]);
                    }
                }
                break;
            }
            case OMERO_PROJECTS: {
                String projects = OmeroProjects();
                ((String[]) args[0])[0] = projects;
                break;
            }
            case OMERO_DATASETS: {
                String project = (String) args[0];
                String datasets = OmeroDatasets(project);
                ((String[]) args[1])[0] = datasets;
                break;
            }
            case OMERO_IMAGES: {
                String project = (String) args[0];
                String dataset = (String) args[1];
                String images = OmeroImages(project, dataset);
                ((String[]) args[2])[0] = images;
                break;
            }
        }

        return null;
    }

    private ImagePlus OmeroLoad(String project, String dataset, String name) {
        OmeroConnect2 connect = new OmeroConnect2();
        ImageWrapper imageWrapper = connect.findOneImage(project, dataset, name, true);
        return connect.getImage(imageWrapper, 1, 1).getImagePlus();
    }

    private void OmeroSave(ImagePlus plus, String project, String dataset, String name) {
        OmeroConnect2 connect = new OmeroConnect2();
        //save tmp file
        String dirTmp = System.getProperty("java.io.tmpdir");
        String pathOmero = dirTmp + File.separator + name;
        if (!saveFileImage(plus, pathOmero)) IJ.log("Pb saving temp " + pathOmero);
        connect.addImageToDataset(project, dataset, dirTmp, name);
    }

    private void OmeroAttach(ResultsTable table, String project, String dataset, String image, String name) {
        OmeroConnect2 connect = new OmeroConnect2();
        try {
            File file = new File(System.getProperty("java.io.tmpdir") + File.separator + name);
            table.saveAs(file.getPath());
            IJ.log("Attaching to OMERO : " + project + "/" + dataset + "/" + image);
            connect.addFileAnnotation(connect.findOneImage(project, dataset, image, true), file);
            if (!file.delete()) System.out.println("PB deleting tmp file " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String OmeroProjects() {
        OmeroConnect2 connect = new OmeroConnect2();
        String s = "";
        try {
            List<ProjectWrapper> projectsData = connect.findAllProjects();
            for (ProjectWrapper data : projectsData) {
                s = s.concat(data.getName()).concat(delimiter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s;
    }

    private String OmeroDatasets(String project) {
        OmeroConnect2 connect = new OmeroConnect2();
        String s = "";
        try {
            List<DatasetWrapper> datasets = connect.findDatasets(connect.findProject(project, true));
            for (DatasetWrapper data : datasets) {
                s = s.concat(data.getName()).concat(delimiter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s;
    }

    private String OmeroImages(String project, String dataset) {
        OmeroConnect2 connect = new OmeroConnect2();
        String s = "";
        try {
            List<ImageWrapper> images = connect.findAllImages(connect.findDataset(dataset, connect.findProject(project, true), true));
            for (ImageWrapper data : images) {
                s = s.concat(data.getName()).concat(delimiter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s;
    }


    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        int[] argOmero = {ARG_STRING, ARG_STRING, ARG_STRING};
        int[] argStringOutput = {ARG_STRING + ARG_OUTPUT};
        int[] argStringStringOutput = {ARG_STRING, ARG_STRING + ARG_OUTPUT};
        int[] argStringStringStringOutput = {ARG_STRING, ARG_STRING, ARG_STRING + ARG_OUTPUT};
        int[] argOmeroTable = {ARG_STRING, ARG_STRING, ARG_STRING, ARG_STRING};
        return new ExtensionDescriptor[]{
                new ExtensionDescriptor(OMERO_LOAD, argOmero, this),
                new ExtensionDescriptor(OMERO_SAVE, argOmero, this),
                new ExtensionDescriptor(OMERO_PROJECTS, argStringOutput, this),
                new ExtensionDescriptor(OMERO_DATASETS, argStringStringOutput, this),
                new ExtensionDescriptor(OMERO_IMAGES, argStringStringStringOutput, this),
                new ExtensionDescriptor(OMERO_RESULTS, argOmeroTable, this),
        };
    }

    private String[] extractArgsOmero(Object[] args) {
        if (args.length < 3) return null;
        return new String[]{(String) args[0], (String) args[1], (String) args[2]};
    }

    private String[] extractArgsOmeroName(Object[] args) {
        if (args.length < 4) return null;
        return new String[]{(String) args[0], (String) args[1], (String) args[2], (String) args[3]};
    }


    private boolean saveFileImage(ImagePlus input, String path) {
        FileSaver saver = new FileSaver(input);
        boolean saveOk;
        if (input.getNSlices() > 1) {
            saveOk = saver.saveAsTiffStack(path);
        } else {
            saveOk = saver.saveAsTiff(path);
        }

        return saveOk;
    }


    @Override
    public void run(String s) {
        IJ.log("Implementing OMERO macros.");
        if (omeroMacro == null) omeroMacro = new OmeroMacro();
    }
}
