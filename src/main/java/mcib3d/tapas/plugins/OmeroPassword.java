package mcib3d.tapas.plugins;

import ij.Prefs;

import java.util.Base64;

public class OmeroPassword {
    private String serverOmero = "server";
    private String userOmero = "user";
    private String passwordOmero = "password";
    private int portOmero = 4064;

    // TEST
    private long group = -1;

    public OmeroPassword() {
    }

    public OmeroPassword(String user, String password, String server, String port) {
        serverOmero = server;
        userOmero = user;
        passwordOmero= password;
        portOmero = Integer.parseInt(port);
    }
    public OmeroPassword(String user, char[] password, String server, String port) {
        serverOmero = server;
        userOmero = user;
        passwordOmero = new String(password);
        portOmero = Integer.parseInt(port);
    }

    public String getServerOmero() {
        return serverOmero;
    }

    public String getUserOmero() {
        return userOmero;
    }

    public char[] getPasswordOmeroArrayChar() {
        return passwordOmero.toCharArray();
    }

    public String getPasswordOmeroString() {
        return passwordOmero;
    }

    public int getPortOmero() {
        return portOmero;
    }

    public long getGroup() {return group;}
    public void setGroup(long gr){group = gr;}

    public void saveConnectionInformation() {
        Prefs.set("OMERO.TB.user.string", userOmero);
        Prefs.set("OMERO.TB.server.string", serverOmero);
        Prefs.set("OMERO.TB.port.int", portOmero);
        String encoded = Base64.getEncoder().encodeToString(passwordOmero.getBytes());
        Prefs.set("ap.id", encoded);
    }

    public void loadConnectionInformation() {
        userOmero = Prefs.get("OMERO.TB.user.string", userOmero);
        serverOmero = Prefs.get("OMERO.TB.server.string", serverOmero);
        portOmero = (int) Prefs.get("OMERO.TB.port.int", portOmero);
        String pass = Prefs.get("ap.id", passwordOmero);
        passwordOmero = new String(Base64.getDecoder().decode(pass));
    }
}
