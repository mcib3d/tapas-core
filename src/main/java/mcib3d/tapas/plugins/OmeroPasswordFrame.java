package mcib3d.tapas.plugins;

import fr.igred.omero.Client;
import ij.IJ;
import ij.Prefs;

import javax.swing.*;
import java.awt.*;

public class OmeroPasswordFrame extends JFrame {

    public OmeroPasswordFrame(String title) throws HeadlessException {
        super(title);
        JPanel panel = new JPanel();
        GridLayout layout = new GridLayout(0, 2, 1, 1);
        JLabel userLabel = new JLabel("User");
        JTextField userField = new JTextField(Prefs.get("OMERO.TB.user.string", "user"), 20);
        JLabel serverLabel = new JLabel("Server");
        JTextField serverField = new JTextField(Prefs.get("OMERO.TB.server.string", "server"), 100);
        JLabel portLabel = new JLabel("Port");
        JTextField portField = new JTextField(Prefs.get("OMERO.TB.port.int", "4064"), 10);
        JLabel passwordlabel = new JLabel("Password");
        JPasswordField passwordField = new JPasswordField(" ", 20);
        JButton cancelButton = new JButton("Cancel");
        JButton okButton = new JButton("OK");
        panel.setLayout(layout);
        panel.add(userLabel);
        panel.add(userField);
        panel.add(serverLabel);
        panel.add(serverField);
        panel.add(portLabel);
        panel.add(portField);
        panel.add(passwordlabel);
        panel.add(passwordField);
        panel.add(okButton);
        panel.add(cancelButton);
        panel.setBackground(new Color(240,232,224));
        add(panel);
        setSize(600, 200);
        setVisible(true);

        okButton.addActionListener(e -> {
            OmeroPassword password = new OmeroPassword(userField.getText(), passwordField.getPassword() , serverField.getText(), portField.getText());
            password.saveConnectionInformation();
            password.loadConnectionInformation();
            IJ.log("Saved connection information "+password.getUserOmero()+" "+password.getServerOmero());
            dispose();

            Client client = new Client();
            try{
                // IGRED
                client.connect(serverField.getText(), Integer.parseInt(portField.getText()), userField.getText(), passwordField.getPassword());
                client.getProjects().forEach(proj -> {
                    IJ.log("Found project : " + proj.getName()+" "+proj.getId());
                });
            } catch (Exception e1) {
                IJ.log(e1.getMessage());
                e1.printStackTrace();
            }
            finally {
                client.disconnect();
                dispose();
            }
        });

        cancelButton.addActionListener(e -> {
            IJ.log("Cancel");
            dispose();
        });
    }
}
