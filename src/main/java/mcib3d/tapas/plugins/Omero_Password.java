package mcib3d.tapas.plugins;

import ij.plugin.PlugIn;
import mcib3d.tapas.utils.CheckInstall;

public class Omero_Password implements PlugIn {
    @Override
    public void run(String arg) {
        // check install
        if(!CheckInstall.installComplete()) return;

        new OmeroPasswordFrame("OMERO connection");
    }
}