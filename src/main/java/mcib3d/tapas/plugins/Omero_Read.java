package mcib3d.tapas.plugins;

import fr.igred.omero.Client;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.PixelsWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.GenericDialog;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.core.OmeroConnect2;
import mcib3d.tapas.utils.CheckInstall;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Omero_Read implements ij.plugin.PlugIn {
    private String project = "project";
    private String dataset = "dataset";
    private String image = "image";
    private int c0 = 1;
    private int c1 = 1;
    private int t0 = 1;
    private int t1 = 1;
    private int binningXY = 1;
    private int binningZ = 1;

    @Override
    public void run(String s) {
        // check install
        if (!CheckInstall.installComplete()) return;
        if (dialog()) {
            Prefs.set("OMEROREAD.project", project);
            Prefs.set("OMEROREAD.dataset", dataset);
            Prefs.set("OMEROREAD.image", image);
            Prefs.set("OMEROREAD.frame0.int", t0);
            Prefs.set("OMEROREAD.frame1.int", t1);
            Prefs.set("OMEROREAD.channel0.int", c0);
            Prefs.set("OMEROREAD.channel1.int", c1);
            Prefs.set("OMEROREAD.binXY.int", binningXY);
            Prefs.set("OMEROREAD.binZ.int", binningZ);

            // GRED
            OmeroConnect2 connect2 = new OmeroConnect2();
            Client omero = connect2.getOmeroClient();
            try {
                // project
                List<ProjectWrapper> projects = omero.getProjects(project);
                if (projects.isEmpty()) {
                    IJ.log("Could not find project " + project);
                    return;
                }
                ProjectWrapper projectWrapper = projects.get(0);
                // dataset
                List<DatasetWrapper> datasets = projectWrapper.getDatasets(dataset);
                if (datasets.isEmpty()) {
                    IJ.log("Could not find dataset " + dataset + " in project " + project);
                    return;
                }
                DatasetWrapper datasetWrapper = datasets.get(0);
                // image
                List<ImageWrapper> images = datasetWrapper.getImages(omero, image);
                if (images.isEmpty()) {
                    IJ.log("Could not find image " + image + " in dataset " + dataset);
                    return;
                }
                ImageWrapper imageWrapper = images.get(0);
                PixelsWrapper pixels = imageWrapper.getPixels();
                if ((binningXY == 1) && (binningZ == 1)) {
                    IJ.log("wrapper " + imageWrapper);
                    ImagePlus plus = imageWrapper.toImagePlus(omero);
                    IJ.log("wrapper " + imageWrapper + " " + plus);
                    ImageHandler handler = ImageHandler.wrap(plus);
                    handler.show();
                } else {
                    ImageHandler handler = ImageHandler.wrap(imageWrapper.toImagePlus(omero, null, null, new int[]{c0 - 1, c1 - 1}, null, new int[]{t0 - 1, t1 - 1}));
                    handler.resample(pixels.getSizeX() / binningXY, pixels.getSizeY() / binningXY, pixels.getSizeZ() / binningZ, -1).show();
                }

            } catch (AccessException | ServiceException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean dialog() {
        // read Prefs
        project = Prefs.get("OMEROREAD.project", "project");
        dataset = Prefs.get("OMEROREAD.dataset", "dataset");
        image = Prefs.get("OMEROREAD.image", "image");
        c0 = Prefs.getInt("OMEROREAD.channel0.int", 1);
        c1 = Prefs.getInt("OMEROREAD.channel0.int", 1);
        t0 = Prefs.getInt("OMEROREAD.frame0.int", 1);
        t1 = Prefs.getInt("OMEROREAD.frame1.int", 1);
        binningXY = Prefs.getInt("OMEROREAD.binXY.int", 1);
        binningZ = Prefs.getInt("OMEROREAD.binZ.int", 1);
        GenericDialog dialog = new GenericDialog("OMERO LOAD");
        dialog.addStringField("Project", project, 50);
        dialog.addStringField("Dataset", dataset, 50);
        dialog.addStringField("Image", image, 50);
        dialog.addMessage("Binning");
        dialog.addNumericField("BinningXY", binningXY, 0);
        dialog.addNumericField("BinningZ", binningZ, 0);
        dialog.addMessage("Channels");
        dialog.addNumericField("First_channel", c0, 0);
        dialog.addNumericField("Last_channel", c1, 0);
        dialog.addMessage("Frames");
        dialog.addNumericField("First_frame", t0, 0);
        dialog.addNumericField("Last_frame", t1, 0);
        dialog.showDialog();

        project = dialog.getNextString();
        dataset = dialog.getNextString();
        image = dialog.getNextString();
        binningXY = (int) dialog.getNextNumber();
        binningZ = (int) dialog.getNextNumber();
        c0 = (int) dialog.getNextNumber();
        c1 = (int) dialog.getNextNumber();
        t0 = (int) dialog.getNextNumber();
        t1 = (int) dialog.getNextNumber();

        return dialog.wasOKed();
    }
}
