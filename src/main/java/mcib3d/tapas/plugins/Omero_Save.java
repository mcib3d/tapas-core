package mcib3d.tapas.plugins;

import fr.igred.omero.Client;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.io.FileSaver;
import ij.plugin.PlugIn;
import mcib3d.tapas.core.OmeroConnect2;
import mcib3d.tapas.utils.CheckInstall;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

public class Omero_Save implements PlugIn {
    private String project = "project";
    private String dataset = "dataset";
    private String image = "image";

    @Override
    public void run(String arg) {
        // check install
        if(!CheckInstall.installComplete()) return;
        if (dialog()) {
            Prefs.set("OMEROSAVE.project", project);
            Prefs.set("OMEROSAVE.dataset", dataset);
            Prefs.set("OMEROSAVE.image", image);
            OmeroConnect2 connect2 = new OmeroConnect2();
            try {
                Client omero = connect2.getOmeroClient();
                // project
                List<ProjectWrapper> projects = omero.getProjects(project);
                if(projects.isEmpty()) {
                    IJ.log("Could not find project " + project);
                    return;
                }
                ProjectWrapper projectWrapper = projects.get(0);
                // dataset
                List<DatasetWrapper> datasets =  projectWrapper.getDatasets(dataset);
                if(datasets.isEmpty()) {
                    IJ.log("Could not find dataset " + dataset + " in project " + project);
                    return;
                }
                DatasetWrapper datasetWrapper = datasets.get(0);

                // SAVE IMAGE LOCALLY
                ImagePlus plus = WindowManager.getCurrentImage();
                FileSaver saver = new FileSaver(plus);
                String pathFile = System.getProperty("user.home") + File.separator + image;
                if (plus.getNSlices() > 1)
                    saver.saveAsTiffStack(pathFile);
                else
                    saver.saveAsTiff(pathFile);

                // check if image already exists in dataset
                List<ImageWrapper> exist = datasetWrapper.getImages(omero, image);
                if(!exist.isEmpty()){
                    IJ.log("Image "+image+" already exists in "+dataset+ ", overwriting");
                    datasetWrapper.removeImage(omero, exist.get(0));
                }
                // IMPORT IMAGE
                IJ.log("Importing "+image+" to OMERO ...");
                datasetWrapper.importImage(omero, pathFile);
                Files.delete(new File(pathFile).toPath());
                IJ.log("Done.");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private boolean dialog() {
        project = Prefs.get("OMEROSAVE.project", "project");
        dataset = Prefs.get("OMEROSAVE.dataset", "dataset");
        image = Prefs.get("OMEROSAVE.image", "image");
        GenericDialog dialog = new GenericDialog("OMERO SAVE");
        dialog.addStringField("Project", project, 50);
        dialog.addStringField("Dataset", dataset, 50);
        dialog.addStringField("Image", image, 50);
        dialog.showDialog();
        project = dialog.getNextString();
        dataset = dialog.getNextString();
        image = dialog.getNextString();
        //strict = dialog.getNextBoolean();

        return dialog.wasOKed();
    }
}

