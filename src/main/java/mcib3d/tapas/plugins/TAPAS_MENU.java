package mcib3d.tapas.plugins;

import ij.IJ;
import ij.WindowManager;
import ij.plugin.BrowserLauncher;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;
import mcib3d.tapas.core.TapasDocumentation;
import mcib3d.tapas.core.TapasProcessingAbstract;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.List;

public class TAPAS_MENU extends JFrame {
    private JComboBox comboBoxPlugins;
    DefaultComboBoxModel<String> comboBoxModel;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JTextField textField8;
    private JTextField textField9;
    private JTextField textField10;
    private JButton createTextButton;
    private JTextArea textArea1;
    private JPanel panel1;
    private JLabel Param1;
    private JLabel Param2;
    private JLabel Param3;
    private JLabel Param4;
    private JLabel Param5;
    private JLabel Param6;
    private JLabel Param7;
    private JLabel Param8;
    private JLabel Param9;
    private JLabel Param10;
    private JTextField textFieldDescription;
    private JButton websiteButton;
    private JTextPane descriptionTAPAS;
    private JCheckBox checkBoxWriteDefault;
    private JComboBox comboBoxCategory;

    TapasDocumentation documentation;

    File tapasFile;
    Map<String, String> plugins;
    Map<String, String> pluginClass;
    int maxParam = 10;
    JTextField paramsText[] = new JTextField[maxParam];
    JLabel paramsLabel[] = new JLabel[maxParam];
    TapasProcessingAbstract currentTapas;
    JLabel selectCategory;
    JLabel selectTapas;

    @Deprecated
    public TAPAS_MENU() {
        // init parameters fields // FIXME dynamic
        paramsText[0] = textField1;
        paramsLabel[0] = Param1;
        paramsText[1] = textField2;
        paramsLabel[1] = Param2;
        paramsText[2] = textField3;
        paramsLabel[2] = Param3;
        paramsText[3] = textField4;
        paramsLabel[3] = Param4;
        paramsText[4] = textField5;
        paramsLabel[4] = Param5;
        paramsText[5] = textField6;
        paramsLabel[5] = Param6;
        paramsText[6] = textField7;
        paramsLabel[6] = Param7;
        paramsText[7] = textField8;
        paramsLabel[7] = Param8;
        paramsText[8] = textField9;
        paramsLabel[8] = Param9;
        paramsText[9] = textField10;
        paramsLabel[9] = Param10;

        // test
        descriptionTAPAS.setContentType("text/html");
        descriptionTAPAS.setText("Documentation will appear here.");

        // read list of Tapas
        tapasFile = TapasBatchUtils.getTapasMenuFile();
        if (tapasFile == null) return;
        plugins = TapasBatchProcess.readPluginsFile(tapasFile.getAbsolutePath(), false);
        pluginClass = new HashMap<>();
        for (String s : plugins.keySet()) {
            System.out.println(plugins.get(s) + " " + s);
            pluginClass.put(plugins.get(s), s);
        }
        java.util.List<String> pluginsName = new ArrayList<>(plugins.size());
        pluginsName.addAll(plugins.keySet());
        Collections.sort(pluginsName);

        // documentation
        documentation = new TapasDocumentation();
        documentation.loadDocumentation(tapasFile.getParent() + File.separator + "tapasDocumentation.txt");
        java.util.List<String> categories = documentation.getCategories();
        Collections.sort(categories);
        comboBoxCategory.removeAllItems();
        for (String category : categories) {
            comboBoxCategory.addItem(category);
        }
        comboBoxCategory.setSelectedIndex(0);

        // fill the combo
        comboBoxModel = new DefaultComboBoxModel<>();
        for (String s : pluginsName) {
            comboBoxModel.addElement(s);
        }
        comboBoxPlugins.setModel(comboBoxModel);

        // display the frame
        panel1.setMinimumSize(new Dimension(800, 600));
        panel1.setPreferredSize(new Dimension(800, 600));
        setContentPane(panel1);
        setTitle("TAPAS MENU " + TapasBatchProcess.version);
        pack();
        setVisible(true);

        // register with Image
        WindowManager.addWindow(this);
        WindowManager.setWindow(this);

        comboBoxPlugins.setSelectedIndex(0);
        selectPlugins();

        initText();

        comboBoxPlugins.addActionListener(e -> selectPlugins());
        createTextButton.addActionListener(e -> createText());
        websiteButton.addActionListener(e -> launchWebsite());
        comboBoxCategory.addActionListener(e -> selectCategories());
    }

    private void launchWebsite() {
        try {
            BrowserLauncher.openURL("https://mcib3d.frama.io/tapas-doc/");
        } catch (IOException e) {
            IJ.log("Cannot find website");
        }
    }


    private void initText() {
        textArea1.setFont(textArea1.getFont().deriveFont(14f)); // will only change size
        String process = "";
        process = process.concat("// first process should be input \n");
        process = process.concat("// to read image from OMERO \n");
        process = process.concat("// or from file \n");
        process = process.concat("process:input \n");
        textArea1.append(process);
        textArea1.append("\n");
    }

    private void createText() {
        String process = "";
        process = process.concat("// " + currentTapas.getName() + "\n");
        process = process.concat("process:" + comboBoxPlugins.getSelectedItem().toString() + "\n");
        // parameters
        String[] parameters = currentTapas.getParameters();
        int np = parameters.length;
        boolean defaultValues = checkBoxWriteDefault.isSelected();
        for (int i = 0; i < np; i++) {
            String text = paramsText[i].getText().trim();
            boolean defaultValue = false;
            if (currentTapas.getParameter(parameters[i]) != null)
                defaultValue = currentTapas.getParameter(parameters[i]).equalsIgnoreCase(text);
            if (defaultValues || !defaultValue)
                process = process.concat(parameters[i] + ":" + paramsText[i].getText() + "\n");
        }
        textArea1.append(process);
        textArea1.append("\n");
    }

    private void selectCategories() {
        String cat = (String) comboBoxCategory.getSelectedItem();
        List<String> plugins = documentation.getPlugingsFromCategory(cat);
        if ((plugins == null) || plugins.isEmpty()) return;
        System.out.println("Selected category " + cat + " has " + plugins.size() + " plugins");

        comboBoxModel.removeAllElements();
        for (String plugin : plugins) {
            comboBoxModel.addElement(pluginClass.get(plugin));
        }
        comboBoxPlugins.setSelectedIndex(0);
        comboBoxPlugins.updateUI();

    }

    private void selectPlugins() {
        String plugin = comboBoxPlugins.getSelectedItem().toString();
        String className = plugins.get(plugin);
        //IJ.log("Selected " + plugin + " " + className);
        // create plugin
        Class cls;
        try {
            cls = Class.forName(className);
            Constructor constructor = cls.getConstructor();
            Object object = constructor.newInstance();
            currentTapas = (TapasProcessingAbstract) object;
            textFieldDescription.setText(currentTapas.getName());
            // documentation
            String doc = documentation.getDocumentation(currentTapas.getClass().getName());
            descriptionTAPAS.setText(doc);

            // parameters
            String[] parameters = currentTapas.getParameters();
            int np = parameters.length;
            for (int i = 0; i < np; i++) {
                paramsLabel[i].setText(parameters[i]);
                paramsLabel[i].setVisible(true);
                paramsText[i].setEnabled(true);
                String par = currentTapas.getParameter(parameters[i]);
                if ((par != null) && (!par.isEmpty())) paramsText[i].setText(par);
                else paramsText[i].setText("");
            }
            for (int i = np; i < maxParam; i++) {
                paramsLabel[i].setText("");
                paramsLabel[i].setVisible(false);
                paramsText[i].setEnabled(false);
                paramsText[i].setText("");
            }
        } catch (ClassNotFoundException e) {
            IJ.log("No class " + className);
        } catch (IllegalAccessException e) {
            IJ.log("Pb class " + className);
        } catch (InstantiationException e) {
            IJ.log("Pb init " + className);
        } catch (NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void createUIComponents() {
    }
}
