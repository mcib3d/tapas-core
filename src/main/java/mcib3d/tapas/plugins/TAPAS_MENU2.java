package mcib3d.tapas.plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.plugin.BrowserLauncher;
import ij.text.TextWindow;
import mcib3d.tapas.core.*;
import mcib3d.tapas.utils.CheckInstall;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class TAPAS_MENU2 extends JFrame {
    private JPanel panel1;
    DefaultComboBoxModel<String> modelCategory = new DefaultComboBoxModel<>();
    private JComboBox comboBoxCategory;
    DefaultComboBoxModel<String> modelPlugin = new DefaultComboBoxModel<>();
    private JComboBox comboBoxPlugin;
    private JTextPane textDocumentation;
    private JPanel panelParameters;
    private JCheckBox writeDefaultValuesCheckBox;
    private JButton addPluginButton;
    private JButton websiteButton;
    private JButton exportTextButton;
    private JTextArea textArea1;
    private JLabel pluginLabel;
    private JButton testModuleButton;

    private JTextField[] paramsText;

    File tapasFile;
    TapasDocumentation documentation;
    Map<String, String> plugins;
    Map<String, String> pluginClass;
    private TapasProcessingAbstract currentTapas;

    public TAPAS_MENU2() {
        // check install
        if (!CheckInstall.installComplete()) return;
        // tapas
        tapasFile = TapasBatchUtils.getTapasMenuFile();
        IJ.log("Tapas File "+tapasFile);
        textDocumentation.setContentType("text/html");
        textDocumentation.setText("Documentation will appear here.");
        if (tapasFile == null) return;
        // read list of Tapas
        tapasFile = TapasBatchUtils.getTapasMenuFile();
        if (tapasFile == null) return;
        plugins = TapasBatchProcess.readPluginsFile(tapasFile.getAbsolutePath(), false);
        pluginClass = new HashMap<>();
        for (String s : plugins.keySet()) {
            System.out.println(plugins.get(s) + " " + s);
            pluginClass.put(plugins.get(s), s);
        }
        List<String> pluginsName = new ArrayList<>(plugins.size());
        pluginsName.addAll(plugins.keySet());
        Collections.sort(pluginsName);
        // documentation
        documentation = new TapasDocumentation();
        documentation.loadDocumentation(tapasFile.getParent() + File.separator + "tapasDocumentation.txt");
        documentation.checkNoDocPlugin(plugins.values().stream().collect(Collectors.toList()));
        // categories
        List<String> categories = documentation.getCategories();
        Collections.sort(categories);
        for (String category : categories) {
            modelCategory.addElement(category);
        }
        modelCategory.setSelectedItem(categories.get(0));
        comboBoxCategory.setModel(modelCategory);

        // plugins
        comboBoxPlugin.setModel(modelPlugin);


        //comboBoxCategory.addActionListener(e -> selectCategories());
        comboBoxCategory.addItemListener(e -> selectCategories());
        //comboBoxPlugin.addActionListener(e -> selectPlugins());
        comboBoxPlugin.addItemListener(e -> selectPlugins());
        websiteButton.addActionListener(e -> launchWebsite());
        addPluginButton.addActionListener(e -> createText());
        exportTextButton.addActionListener(e -> exportText());

        // panel parameters
        panelParameters.setLayout(new GridLayout(10, 1));

        // initial text
        initText();

        // display the frame
        panel1.setMinimumSize(new Dimension(800, 1000));
        panel1.setPreferredSize(new Dimension(800, 1000));
        setContentPane(panel1);
        setTitle("TAPAS MENU " + TapasBatchProcess.version);
        pack();
        setVisible(true);

        // first time
        selectCategories();
        testModuleButton.addActionListener(e -> testModule());
    }

    private void testModule() {
        // get current image
        ImagePlus currentImage = WindowManager.getCurrentImage();
        // get selected module and apply parameters
        String[] parameters = currentTapas.getParameters();
        int np = parameters.length;
        for (int i = 0; i < np; i++) {
            String paramValue = paramsText[i].getText().trim();
            if (!currentTapas.setParameter(parameters[i], paramValue)) return;
        }
        // run module
        ImageInfo info = new ImageInfo("project", "dataset", currentImage.getTitle());
        currentTapas.setCurrentImage(info);
        ImagePlus result = (ImagePlus) currentTapas.execute(currentImage);
        if (result != null) {
            result.setTitle(currentImage.getTitle() + "_" + comboBoxPlugin.getSelectedItem().toString());
            result.show();
        }
    }

    private void initText() {
        textArea1.setFont(textArea1.getFont().deriveFont(14f)); // will only change size
        String process = "";
        process = process.concat("// first process should be input \n");
        process = process.concat("// to read image from OMERO \n");
        process = process.concat("// or from file \n");
        process = process.concat("process:input \n");
        textArea1.append(process);
        textArea1.append("\n");
    }

    private void launchWebsite() {
        try {
            BrowserLauncher.openURL("https://mcib3d.frama.io/tapas-doc/");
        } catch (IOException e) {
            IJ.log("Cannot find website");
        }
    }

    private void createText() {
        String process = "";
        process = process.concat("// " + currentTapas.getName() + "\n");
        process = process.concat("process:" + comboBoxPlugin.getSelectedItem().toString() + "\n");
        // parameters
        String[] parameters = currentTapas.getParameters();
        int np = parameters.length;
        boolean defaultValues = writeDefaultValuesCheckBox.isSelected();
        for (int i = 0; i < np; i++) {
            String text = paramsText[i].getText().trim();
            boolean defaultValue = false;
            if (currentTapas.getParameter(parameters[i]) != null)
                defaultValue = currentTapas.getParameter(parameters[i]).equalsIgnoreCase(text);
            if (defaultValues || !defaultValue)
                process = process.concat(parameters[i] + ":" + paramsText[i].getText() + "\n");
        }
        textArea1.append(process);
        textArea1.append("\n");
    }

    private void exportText() {
        TextWindow textWindow = new TextWindow("Tapas Workflow", textArea1.getText(), 320, 200);
        textWindow.setVisible(true);
    }

    private void selectCategories() {
        String cat = (String) modelCategory.getSelectedItem();
        List<String> plugins = documentation.getPlugingsFromCategory(cat);
        System.out.println(" * Selected category " + cat + " has " + documentation.getPlugingsFromCategory(cat).size() + " plugins");

        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        if (plugins.size() > 0) {
            for (String plugin : plugins) {
                comboBoxModel.addElement(pluginClass.get(plugin));
            }
            comboBoxPlugin.setModel(comboBoxModel);
            comboBoxPlugin.setSelectedIndex(0);
            modelPlugin = comboBoxModel;
            selectPlugins();
        }
    }

    private void selectPlugins() {
        String plugin = modelPlugin.getSelectedItem().toString();
        String className = plugins.get(plugin);
        System.out.println("Selected " + plugin + " " + className);
        pluginLabel.setText("Selected plugin : " + plugin);
        // create plugin
        Class cls;
        try {
            cls = Class.forName(className);
            Constructor constructor = cls.getConstructor();
            Object object = constructor.newInstance();
            currentTapas = (TapasProcessingAbstract) object;
            //textDocumentation.setText(currentTapas.getName());
            // documentation
            String doc = documentation.getDocumentation(currentTapas.getClass().getName());
            textDocumentation.setText(doc);
            createParameters();
            panelParameters.setVisible(true);
        } catch (ClassNotFoundException e) {
            IJ.log("No class " + className);
            panelParameters.removeAll();
            textDocumentation.setText(TapasDocumentation.DEFAULT);
            panelParameters.updateUI();
        } catch (IllegalAccessException e) {
            IJ.log("Pb class " + className);
            textDocumentation.setText(TapasDocumentation.DEFAULT);
            panelParameters.removeAll();
            panelParameters.updateUI();
        } catch (InstantiationException e) {
            IJ.log("Pb init " + className);
            textDocumentation.setText(TapasDocumentation.DEFAULT);
            panelParameters.removeAll();
            panelParameters.updateUI();
        } catch (NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void createParameters() {
        // parameters
        panelParameters.removeAll();
        String[] parameters = currentTapas.getParameters();
        int np = parameters.length;
        System.out.println("Plugin " + currentTapas.getName() + " has " + np + " parameters");
        paramsText = new JTextField[np];
        for (int i = 0; i < np; i++) {
            JPanel panel = new JPanel();
            panel.setBackground(new Color(240, 232, 224));
            panel.setLayout(new GridLayout(1, 2));
            JLabel paramLabel = new JLabel(parameters[i] + "  :  ", SwingConstants.RIGHT);
            paramLabel.setVisible(true);
            panel.add(paramLabel);
            JTextField paramText = new JTextField(currentTapas.getParameter(parameters[i]), 128);
            paramsText[i] = paramText;
            paramText.setVisible(true);
            panel.add(paramText);
            panelParameters.add(panel);
        }
        for (int i = np; i < 10; i++) {
            JPanel panel = new JPanel();
            panel.setBackground(new Color(240, 232, 224));
            panel.setLayout(new GridLayout(1, 2));
            JLabel paramLabel = new JLabel("");
            paramLabel.setVisible(true);
            panel.add(paramLabel);
            panel.add(paramLabel);
            panelParameters.add(panel);
        }
    }
}
