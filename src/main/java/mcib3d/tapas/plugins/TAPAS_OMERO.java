package mcib3d.tapas.plugins;

import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ImageWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.WindowManager;
import ij.plugin.frame.Recorder;
import mcib3d.tapas.IJ.TapasProcessorIJ;
import mcib3d.tapas.core.*;
import mcib3d.tapas.dev.JobsGenerate;
import mcib3d.tapas.dev.Jobs_Scripts;
import mcib3d.tapas.utils.CheckInstall;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TAPAS_OMERO extends JFrame {
    private JPanel panel1;
    private JTextField textFieldProject;
    private JComboBox comboProjects;
    private JTextField textFieldDataset;
    private JComboBox comboDatasets;
    private JTextField textFieldImage;
    private JList listImages;
    //private JTextField textFieldChannel;
    //private JTextField textFieldFrame;
    private JTextField textFieldProcess;
    private JButton browseButton;
    private JButton runProcessingButton;
    private JTextField textFieldRoot;
    private JButton jobsButton;
    private JTextField textFieldTag;
    private JCheckBox jobsCheckBox;
    //
    //OmeroConnect omero;
    OmeroConnect2 connect2 = new OmeroConnect2();
    DefaultListModel model = new DefaultListModel();
    File tapasFile;
    // processor
    TapasProcessorAbstract processor;

    public static void main(String args[]) {

    }

    public TapasProcessorAbstract getProcessor() {
        return processor;
    }

    public void setProcessor(TapasProcessorAbstract processorAbstract) {
        this.processor = processorAbstract;
    }

    public TAPAS_OMERO() {
        // check install
        if (!CheckInstall.installComplete()) return;
        // by default IJ processor
        setProcessor(new TapasProcessorIJ());
        tapasFile = TapasBatchUtils.getTapasMenuFile();
        if (tapasFile == null) return;
        IJ.log("Getting list of projects and datasets from OMERO, please wait ...");
        listImages.setModel(model);
        //textFieldFrame.setText("0-0");
        //textFieldChannel.setText("0-0");
        // check jobs ?
        ClassLoader loader = IJ.getClassLoader();
        try {
            loader.loadClass("mcib3d.tapas.dev.Jobs_Scripts");
            //IJ.log("Found Job plugin");
            //jobsButton.setVisible(true);
            jobsButton.addActionListener(e -> generateJobs());
        } catch (Exception e) {
            jobsButton.setVisible(false);
        }
        panel1.setMinimumSize(new Dimension(800, 600));
        panel1.setPreferredSize(new Dimension(800, 600));
        setContentPane(panel1);
        setMinimumSize(new Dimension(800, 600));
        setPreferredSize(new Dimension(800, 600));
        setTitle("TAPAS OMERO " + TapasBatchProcess.version);
        pack();
        setVisible(true);

        // register with Image
        WindowManager.addWindow(this);
        WindowManager.setWindow(this);

        // fill projects
        try {
            // projects
            List<ProjectWrapper> projects = connect2.findAllProjects();
            projects.sort(new compareProject());
            projects.forEach(P -> comboProjects.addItem(P.getName()));
            textFieldProject.setText(projects.get(0).getName());
            selectProject();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // browse project file
        browseButton.addActionListener(e -> browseProcess());

        // update projects
        comboProjects.addActionListener(e -> selectProject());

        // update datasets
        comboDatasets.addActionListener(e -> selectDataset());

        // update images
        listImages.addListSelectionListener(e -> selectImage());

        // processing button
        runProcessingButton.addActionListener(e -> {
            runProcessingButton.setEnabled(false);
            processing();
        });
        // TAG
        textFieldTag.addActionListener(actionEvent -> selectDataset());
    }

    private void browseRoot() {
        String file = IJ.getFilePath("Choose root");
        textFieldRoot.setText(file);
    }

    private void browseProcess() {
        String file = IJ.getFilePath("Choose process");
        textFieldProcess.setText(file);
    }

    private void selectProject() {
        String project = comboProjects.getSelectedItem().toString();
        textFieldProject.setText(project);
        // fill datasets
        try {
            // project
            ProjectWrapper projectData = connect2.findProject(project, true);
            List<DatasetWrapper> datasets = connect2.findDatasets(projectData);
            datasets.sort(new compareDataset());
            comboDatasets.removeAllItems();
            for (int i = 0; i < datasets.size(); i++) {
                comboDatasets.addItem(datasets.get(i).getName());
            }
            if (!datasets.isEmpty())
                textFieldDataset.setText(datasets.get(0).getName());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void selectDataset() {
        // get tag
        String tag = textFieldTag.getText().trim();
        // get project and dataset
        String project = comboProjects.getSelectedItem().toString();
        String dataset = "";
        if (comboDatasets.getSelectedItem() != null) {
            dataset = comboDatasets.getSelectedItem().toString();
            textFieldDataset.setText(dataset);
            // fill images
            try {
                // project
                ProjectWrapper projectData = connect2.findProject(project, true);
                DatasetWrapper datasetData = connect2.findDataset(dataset, projectData, true);
                List<ImageWrapper> allImages = connect2.findAllImages(datasetData);
                List<ImageWrapper> images = new ArrayList<>();
                for (ImageWrapper image : allImages) {
                    if ((tag.isEmpty()) || (connect2.hasTag(image, tag))) {
                        images.add(image);
                    }
                }
                images.sort(new compareImages());
                model.removeAllElements();
                for (ImageWrapper image : images) {
                    model.addElement(image.getName());
                }
                textFieldImage.setText("");
                listImages.updateUI();
                listImages.repaint();
                repaint();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void selectImage() {
        int[] indices = listImages.getSelectedIndices();
        String text;
        if (indices.length == 1) text = model.get(indices[0]).toString();
        else text = indices.length + " images selected";
        textFieldImage.setText(text);
    }

    private List<String> listImages() {
        int[] indices = listImages.getSelectedIndices();
        List<String> images = new ArrayList<>();
        if (indices.length == 0) {
            String rep = IJ.getString("Process all files (y/n) ?", "n");
            if (rep.equalsIgnoreCase("y")) {
                indices = new int[model.getSize()];
                for (int i = 0; i < model.getSize(); i++) {
                    indices[i] = i;
                }
            } else {
                runProcessingButton.setEnabled(true);
                return images;
            }
        }
        if ((indices.length >= 1) && (indices.length <= model.size())) {
            for (int i = 0; i < indices.length; i++) {
                images.add(model.get(indices[i]).toString());
            }
        }

        return images;
    }

    private void processing() {
        // batch process
        TapasBatchProcess batchProcess = new TapasBatchProcess();
        String project = textFieldProject.getText();
        String dataset = textFieldDataset.getText();
        // images
        String image = "";
        List<String> images = listImages();
        for (int i = 0; i < images.size() - 1; i++) {
            image = image.concat(images.get(i) + ",");
        }
        image = image.concat(images.get(images.size() - 1));

        String imageFinal = image;
        String processFile = textFieldProcess.getText();
        if (!batchProcess.init(processFile, tapasFile.getAbsolutePath())) {
            IJ.log("Aborting");
            return;
        }
        // get processor
        processor = TapasBatchProcess.getProcessor(processFile);
        IJ.log("Processing with " + processor.getNameProcessor());
        setProcessor(processor);
        // channel
        //String channel = textFieldChannel.getText();
        //int[] channels = processTextForTimeChannel(channel);
        int cmin = 1;
        int cmax = 1;
        // frame
        //String frame = textFieldFrame.getText();
        //int[] frames = processTextForTimeChannel(frame);
        int tmin = 1;
        int tmax = 1;
        // init to find images
        System.out.println("Launch thread");
        Thread thread = new Thread(() -> {
            batchProcess.setProcessor(processor);
            try {
                batchProcess.initBatchOmero(project, dataset, imageFinal, cmin, cmax, tmin, tmax);
            } catch (ServiceException e) {
                throw new RuntimeException(e);
            } catch (AccessException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
            batchProcess.processAllImages();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                runProcessingButton.setEnabled(true);
                System.out.println("Error thread " + e);
                e.printStackTrace();
            }
            SwingUtilities.invokeLater(() -> {
                IJ.log("Done");
                // Macro
                if (Recorder.record) {
                    Recorder.setCommand(null);
                    Recorder.record("run", "TAPAS BATCH", "root=[OMERO] project=[" + project + "] dataset=[" + dataset + "] image=[" + imageFinal
                            + "] channel=[" + cmin + "-" + cmax + "] frame=[" + tmin + "-" + tmax + "] processing=[" + processFile + "]");
                }
            });
        });
        thread.start();
        runProcessingButton.setEnabled(true);
    }

    private void generateJobs() {
        JobsGenerate jobsGenerate = new JobsGenerate();
        jobsGenerate.setDataset(comboDatasets.getSelectedItem().toString());
        jobsGenerate.setProject(comboProjects.getSelectedItem().toString());
        jobsGenerate.setImageList(listImages());
        jobsGenerate.setProcessing(textFieldProcess.getText());
        new Jobs_Scripts(jobsGenerate);
    }

    class compareProject implements Comparator<ProjectWrapper> {
        @Override
        public int compare(ProjectWrapper o1, ProjectWrapper o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    }

    class compareDataset implements Comparator<DatasetWrapper> {
        @Override
        public int compare(DatasetWrapper o1, DatasetWrapper o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    }

    class compareImages implements Comparator<ImageWrapper> {
        @Override
        public int compare(ImageWrapper o1, ImageWrapper o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    }

    private int[] processTextForTimeChannel(String nextString) {
        int[] vals = new int[2];
        if (nextString.contains("-")) {
            String[] cs = nextString.split("-");
            vals[0] = Integer.parseInt(cs[0]);
            vals[1] = Integer.parseInt(cs[1]);
        } else {
            vals[0] = Integer.parseInt(nextString);
            vals[1] = vals[0];
        }

        return vals;
    }
}


