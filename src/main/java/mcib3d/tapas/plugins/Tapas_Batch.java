package mcib3d.tapas.plugins;

import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.ServiceException;
import ij.IJ;
import ij.Macro;
import ij.Prefs;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;
import mcib3d.tapas.utils.CheckInstall;

import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Tapas_Batch implements PlugIn {
    private String root = "OMERO";
    private String project = "";
    private String dataset = "";
    private String image = "";
    private String processFile = "";
    private int cmin = 1;
    private int cmax = 1;
    private int tmin = 1;
    private int tmax = 1;

    public static void main(String[] args) {

    }

    @Override
    public void run(String s) {
        // check install
        if (!CheckInstall.installComplete()) return;
        System.out.println("ARG " + IJ.isMacro() + " " + IJ.macroRunning() + " " + s);
        IJ.log("TAPAS BATCH");
        if (!CheckInstall.installComplete()) IJ.log("Please check installation.");

        // read Prefs
        root = Prefs.get("OMEROBATCH.root", root);
        project = Prefs.get("OMEROBATCH.project", "Project");
        dataset = Prefs.get("OMEROBATCH.dataset", "Dataset");
        image = Prefs.get("OMEROBATCH.image", "*");
        processFile = Prefs.get("OMEROBATCH.process", "");
        cmin = (int) Prefs.get("OMEROBATCH.cmin", 1);
        cmax = (int) Prefs.get("OMEROBATCH.cmax", 1);
        tmin = (int) Prefs.get("OMEROBATCH.tmin", 1);
        tmax = (int) Prefs.get("OMEROBATCH.tmax", 1);

        File tapasFile = TapasBatchUtils.getTapasMenuFile();
        if (tapasFile == null) {
            return;
        }

        // TEST HEADLESS
        if (IJ.macroRunning()) {
            System.out.println("HEADLESS " + Macro.getOptions());
            Map<String, String> map = processMacroArguments(Macro.getOptions());

            root = map.get("root");
            project = map.get("project");
            dataset = map.get("dataset");
            image = map.get("image");

            System.out.println("Macro " + root + " " + project + " " + dataset);

            int[] channels = processTextForTimeChannel(map.get("channel"));
            cmin = channels[0];
            cmax = channels[1];
            int[] frames = processTextForTimeChannel(map.get("frame"));
            tmin = frames[0];
            tmax = frames[1];
            processFile = map.get("processing");
            TapasBatchProcess batchProcess = new TapasBatchProcess();
            if (!batchProcess.init(processFile, tapasFile.getAbsolutePath())) {
                IJ.log("Pb with processing file, aborting");
                return;
            }

            // init to find images
            if (root.equalsIgnoreCase("OMERO")) {
                try {
                    batchProcess.initBatchOmero(project, dataset, image, cmin, cmax, tmin, tmax);
                } catch (ServiceException e) {
                    throw new RuntimeException(e);
                } catch (AccessException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
            else batchProcess.initBatchFiles(root, project, dataset, image, cmin, cmax, tmin, tmax);
            // batch process
            batchProcess.processAllImages();
        } // DIALOG
        else if (dialog()) {
            System.out.println("DIALOG");
            TapasBatchProcess batchProcess = new TapasBatchProcess();
            if (!batchProcess.init(processFile, tapasFile.getAbsolutePath())) {
                IJ.log("Pb with processing file, aborting");
                return;
            }
            if (IJ.isMacro()) {
                IJ.log("Macro mode ON for image " + image);
            }

            // init to find images
            if (root.equalsIgnoreCase("OMERO")) {
                try {
                    batchProcess.initBatchOmero(project, dataset, image, cmin, cmax, tmin, tmax);
                } catch (ServiceException e) {
                    throw new RuntimeException(e);
                } catch (AccessException e) {
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
            else batchProcess.initBatchFiles(root, project, dataset, image, cmin, cmax, tmin, tmax);
            // batch process
            batchProcess.processAllImages();

            // save prefs
            Prefs.set("OMEROBATCH.root", root);
            Prefs.set("OMEROBATCH.project", project);
            Prefs.set("OMEROBATCH.dataset", dataset);
            Prefs.set("OMEROBATCH.image", image);
            Prefs.set("OMEROBATCH.process", processFile);
            Prefs.set("OMEROBATCH.cmin", cmin);
            Prefs.set("OMEROBATCH.cmax", cmax);
            Prefs.set("OMEROBATCH.tmin", tmin);
            Prefs.set("OMEROBATCH.tmax", tmax);
        }
    }

    private Map<String, String> processMacroArguments(String arg) {
        Map<String, String> map = new HashMap<>();
        String[] args = arg.split("]");
        for (int i = 0; i < args.length; i++) {
            String arg0 = args[i].trim().replace("[", "");
            int pos = arg0.indexOf("=");
            if (pos < 0) continue;
            map.put(arg0.substring(0, pos), arg0.substring(pos + 1));
        }

        return map;
    }

    private boolean dialog() {
        GenericDialog dialog = new GenericDialog("TAPAS BATCH");
        dialog.setBackground(new Color(240, 232, 224));
        dialog.addStringField("Root", root, 50);
        dialog.addStringField("Project", project, 50);
        dialog.addStringField("Dataset", dataset, 50);
        dialog.addStringField("Image", image, 50);
        //dialog.addStringField("exclude", exclude, 100);
        //dialog.addStringField("Channel", cmin + "-" + cmax, 50);
        //dialog.addStringField("Frame", tmin + "-" + tmax, 50);
        dialog.addStringField("Processing", processFile, 100);
        dialog.showDialog();
        root = dialog.getNextString();
        project = dialog.getNextString().trim();
        dataset = dialog.getNextString().trim();
        image = dialog.getNextString().trim();
        //int[] vals = processTextForTimeChannel(dialog.getNextString());
        //cmin = vals[0];
        //cmax = vals[1];
        //vals = processTextForTimeChannel(dialog.getNextString());
        //tmin = vals[0];
        //tmax = vals[1];
        processFile = dialog.getNextString();

        return dialog.wasOKed();
    }

    private int[] processTextForTimeChannel(String nextString) {
        System.out.println("Tapas Batch " + nextString);
        int[] vals = new int[2];
        if (nextString.contains("-")) {
            String[] cs = nextString.split("-");
            vals[0] = Integer.parseInt(cs[0]);
            vals[1] = Integer.parseInt(cs[1]);
        } else {
            vals[0] = Integer.parseInt(nextString);
            vals[1] = vals[0];
        }

        return vals;
    }
}
