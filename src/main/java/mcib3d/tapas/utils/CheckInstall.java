/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib3d.tapas.utils;

import ij.IJ;

public class CheckInstall {

    public static boolean installComplete() {
        IJ.log("Checking installation for Tapas ...");
        boolean complete = true;
        ClassLoader loader = ij.IJ.getClassLoader();
        try {
            loader.loadClass("mcib3d.image3d.ImageHandler");
        } catch (Exception e) {
            ij.IJ.log("3D Suite not installed. Please install from Update Site in Fiji.");
            complete = false;
        }
        try {
            loader.loadClass("org.scijava.vecmath.Point3f");
            //ij.IJ.log("Replacement of Java3D installed. ");
        } catch (Exception e) {
            ij.IJ.log("Replacement of Java3D (org.scijava) not installed. ");
        }
        try {
            loader.loadClass("imagescience.image.Image");
        } catch (Exception e) {
            //ij.IJ.log("Imagescience not installed, please install from\nhttp://www.imagescience.org/meijering/software/featurej/");
            //complete = false;
        }
        try {
            loader.loadClass("fr.igred.omero.Client");
        } catch (Exception e) {
            ij.IJ.log("SimpleOmeroClient not installed, please install from" +
                    "\nhttps://github.com/GReD-Clermont/simple-omero-client/releases");
            complete = false;
        }
        try {
            loader.loadClass("org.openmicroscopy.shoola.MainIJPlugin");
        } catch (Exception e) {
            ij.IJ.log("OMERO plugin not installed, please install omero-insight for Fiji, even if you do not have an OMERO server." +
                    "\nhttps://www.openmicroscopy.org/omero/downloads/");
            complete = false;
        }
        try {
            loader.loadClass("ome.xml.meta.OMEXMLMetadata");
        } catch (Exception e) {
            ij.IJ.log("BioFormats not installed, please install.");
            complete = false;
        }

        return complete;
    }
}
