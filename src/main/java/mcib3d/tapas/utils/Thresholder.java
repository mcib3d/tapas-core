package mcib3d.tapas.utils;

import ij.IJ;
import ij.ImagePlus;
import ij.process.AutoThresholder;
import mcib3d.image3d.ImageHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Thresholder {
    public static Map<String, AutoThresholder.Method> methods = Collections.unmodifiableMap(new HashMap<String, AutoThresholder.Method>() {{
        put("isodata", AutoThresholder.Method.IsoData);
        put("otsu", AutoThresholder.Method.Otsu);
        put("minimum", AutoThresholder.Method.Minimum);
        put("minerror", AutoThresholder.Method.MinError);
        put("moments", AutoThresholder.Method.Moments);
        put("intermodes", AutoThresholder.Method.Intermodes);
        put("yen", AutoThresholder.Method.Yen);
        put("triangle", AutoThresholder.Method.Triangle);
        put("mean", AutoThresholder.Method.Mean);
        put("renyientropy", AutoThresholder.Method.RenyiEntropy);
        put("shanbag", AutoThresholder.Method.Shanbhag);
        put("triangle", AutoThresholder.Method.Triangle);
        put("yen", AutoThresholder.Method.Yen);
        put("huang", AutoThresholder.Method.Huang);
        put("percentile", AutoThresholder.Method.Percentile);
        put("maxentropy", AutoThresholder.Method.MaxEntropy);
        put("ij_isodata", AutoThresholder.Method.IJ_IsoData);
    }});

    public static float getThreshold(ImageHandler handler, String threshold) {
        // only number --> normal threshold value
        try {
            float th = Float.parseFloat(threshold);
            return th;
        } catch (NumberFormatException e) {
            // contains % --> percentile
            if (threshold.contains("%")) {
                double percent = Double.parseDouble(threshold.substring(0, threshold.indexOf("%")).trim());
                return (float) handler.getPercentile(percent / 100.0, null);
            }
            // name --> autoThresholder
            else {
                return AutoThresholderUtils.getThreshold(handler.getImagePlus(), methods.get(threshold.toLowerCase().trim()).toString());
                // compute histogram
//                ImageStats stat = handler.getImageStats(null);
//                int[] histo = stat.getHisto256();
//                double binSize = stat.getHisto256BinSize();
//                double min = stat.getMin();
//
//                AutoThresholder at = new AutoThresholder();
//                AutoThresholder.Method method = methods.get(threshold.toLowerCase().trim());
//                if (method == null) return Float.NaN;
//
//                double th = at.getThreshold(method, histo);
//                if (handler instanceof ImageByte) return (float) th;
//                else return (float) (th * binSize + min);
            }
        }
    }

    public static void main(String[] args) {
        ImagePlus plus = IJ.openImage("/home/thomas/AMU/DATA/Misc/t1-head.tif");
        ImageHandler handler = ImageHandler.wrap(plus);
        System.out.println("Normal " + getThreshold(handler, "12345"));
        System.out.println("Percent " + getThreshold(handler, "1.2%"));
        System.out.println("Auto " + getThreshold(handler, "otsu"));

        System.out.println("AT " + AutoThresholderUtils.getThreshold(plus, "Otsu"));
    }
}
